//
//  Atomic.swift
//  Nightlight
//
//  Created by Zakk Hoyt on 1/12/20.
//  Copyright © 2020 Zakk Hoyt. All rights reserved.
//

import Foundation

@propertyWrapper
public class Atomic<Value> {
    private let queue: DispatchQueue
    private var value: Value

    /// Syntax for iVar init: @Atomic(wrappedValue: 3, queue: DispatchQueue(label: "test")) var viewCount: Int
    public init(wrappedValue: Value, queue: DispatchQueue) {
        self.value = wrappedValue
        self.queue = queue
    }
    
    public var wrappedValue: Value {
        get { queue.sync { value } }
        set { queue.sync { value = newValue } }
    }
    
    /// This allows us to read and write in a single operetion
    public func mutate(_ mutation: (inout Value) -> Void) {
        return queue.sync {
            mutation(&value)
        }
    }
}
