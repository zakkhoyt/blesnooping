//
//  UserDefaultWrapper.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/4/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation

/// @ZUserDefaults(key: "testKey", wrappedValue: "33")
/// var testValue: String
@propertyWrapper
public class ZUserDefaults<Value> {
    private let key: String
    private var value: Value
    private let userDefaults: UserDefaults

    public init(key: String,
                wrappedValue: Value,
                userDefaults: UserDefaults = UserDefaults.standard) {
        self.key = key
        self.value = wrappedValue
        self.userDefaults = userDefaults
    }
    
    public var wrappedValue: Value {
        get { userDefaults.object(forKey: key) as? Value ?? value }
        set { userDefaults.set(newValue, forKey: key) }
    }
}
