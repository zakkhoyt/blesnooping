//
//  StoryboardInitializable.swift
//  Nightlight
//
//  Created by Ron Lee on 5/21/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit

/// Capability for initialization from storyboard
public protocol StoryboardInitializable {
    /// Name of storyboard to use for initialization
    static var storyboardName: String { get }
    /// String for the storyboard ID property set for the view controller in the storyboard
    static var storyboardIdentifier: String? { get }

    static func storyboard() -> UIStoryboard
    
//    static func loadViewControllerStoryboard() -> Self
}

extension StoryboardInitializable where Self: UIViewController {
    public static func storyboard() -> UIStoryboard {
        UIStoryboard(name: storyboardName, bundle: nil)
    }
    
    /// Instantiates from storyboard. If storyboardIdentifier is set, this calls instantiateViewController(withIdentifier:). Otherwise this calls instantiateInitialViewController().
    public static func loadFromStoryboard() -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)

        let viewController: Self?
        if let storyboardIdentifier = self.storyboardIdentifier {
            viewController = storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as? Self
        } else {
            viewController = storyboard.instantiateInitialViewController() as? Self
        }

        guard let safelyUnwrappedVC = viewController else {
            assertionFailure("Failed to load viewController \(self) from storyboard name \(storyboardName)")
            return self.init()
        }

        return safelyUnwrappedVC
    }
    
    /// Instantiates from storyboard. If storyboardIdentifier is set, this calls instantiateViewController(withIdentifier:). Otherwise this calls instantiateInitialViewController().
    public static func loadFromStoryboard(creator: ((NSCoder) -> Self?)?) -> Self {
        let storyboard = UIStoryboard(name: storyboardName, bundle: Bundle.main)

        let viewController: Self?
        if let storyboardIdentifier = self.storyboardIdentifier {
            viewController = storyboard.instantiateViewController(identifier: storyboardIdentifier, creator: creator)
        } else {
            viewController = storyboard.instantiateInitialViewController(creator: creator)
        }

        guard let safelyUnwrappedVC = viewController else {
            assertionFailure("Failed to load viewController \(self) from storyboard name \(storyboardName)")
            return self.init()
        }

        return safelyUnwrappedVC
    }
}
