//
//  UserDefaults.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

public class PublicUserDefaults {
    private static let kPreviousDeviceKey = "previousDevice"

    public static func writePreviousDevice(device: BLEDevice) {
        let previousBLEDevice = PreviousBLEDevice(device: device)
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: previousBLEDevice, requiringSecureCoding: false)
            Foundation.UserDefaults.standard.set(data, forKey: PublicUserDefaults.kPreviousDeviceKey)
            Logger.debug("UserDefaults", "Set previous device")
        } catch {
            Logger.debug("UserDefaults", "Failed to set previous device")
        }
    }
    
    public static func readPreviousDevice() -> PreviousBLEDevice? {
        guard let data = Foundation.UserDefaults.standard.object(forKey: PublicUserDefaults.kPreviousDeviceKey) as? Data else { return nil }
        do {
            return try NSKeyedUnarchiver.unarchivedObject(ofClass: PreviousBLEDevice.self, from: data)
        } catch {
            Logger.debug("UserDefaults", "Failed to read previous device")
            return nil
        }
    }
}

extension PublicUserDefaults {
    private static let kFilterKey = "filter"

    public static func writeFilter(_ filter: BLEFilter) {
        do {
            let data = try JSONEncoder().encode(filter)
            Foundation.UserDefaults.standard.set(data, forKey: PublicUserDefaults.kFilterKey)
            Logger.debug("UserDefaults", "Set filter")
        } catch {
            Logger.debug("UserDefaults", "Failed to set filter")
        }
    }
    
    public static func readFilter() -> BLEFilter? {
        guard let data = Foundation.UserDefaults.standard.object(forKey: PublicUserDefaults.kFilterKey) as? Data else {
            Logger.debug("UserDefaults", "No filter stored")
            return nil
        }
        
        do {
            let filter = try JSONDecoder().decode(BLEFilter.self, from: data)
            Logger.debug("UserDefaults", "Decoded filter")
            return filter
        } catch {
            Logger.debug("UserDefaults", "Failed to decode filter")
            return nil
        }
    }
}

public class PeripheralUserDefaults {
    private let userDefaults = UserDefaults(suiteName: "PeripheralNames")
    static let shared = PeripheralUserDefaults()

    public func remove(uuidString: String) {
        userDefaults?.removeObject(forKey: uuidString)
    }
    
    public func write(uuidString: String, name: String) {
        userDefaults?.set(name, forKey: uuidString)
    }
    
    public func name(uuidString: String) -> String? {
        userDefaults?.object(forKey: uuidString) as? String
    }
}
