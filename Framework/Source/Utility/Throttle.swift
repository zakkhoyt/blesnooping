//
//  Throttle.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation

public class ThrottleLatest<T> {
    /// Duration between throttled callbacks
    public private(set) var duration: TimeInterval = 0.5
    
    /// Closure to use as a callback mechanism
    private var throttledEvent: ((T) -> Void)?
    
    /// Strong reference to our timer
    private var timer: Timer?
    
    /// A place to store values as they come in.
    private var values: [T] = []
    
    deinit {
        timer?.invalidate()
    }
    
    public init() {}
    
    /// Call start to begin throttling events. Each subsequent call to event(0 will restart a timer for duration seconds.
    /// If that timer expires, then throggledEvent{} is called.
    /// - Parameters:
    ///   - duration: Minimum duration between calls to throttleEvent
    ///   - throttledEvent: A completion handler which called in a throttled fashion
    public func start(duration: TimeInterval,
                      throttledEvent: @escaping (T) -> Void) {
        self.duration = duration
        self.throttledEvent = throttledEvent
    }
    
    public func throttle(name: String? = #function,
                         value: T) {
        if self.timer == nil {
            // fire callback immediately then schedule timer to check for more events
            //            print("ThrottleLatest: fire immediate \(value) \(name ?? "")")
            throttledEvent?(value)
            
            func timerFired(timer: Timer) {
                guard let lastValue = values.last else {
                    self.timer?.invalidate()
                    self.timer = nil
                    return
                }
                values.removeAll()
                //                print("ThrottleLatest: fire throttled \(lastValue) \(name ?? "")")
                throttledEvent?(lastValue)
            }
            
            timer = Timer.scheduledTimer(withTimeInterval: duration, repeats: true, block: timerFired(timer:))
        } else {
            // wait for timer to expire, then call completion handler with eventTimes.last
            values.append(value)
        }
    }
    
    /// Fire event callback without throttling. This is for compatablity with sliderOnly style
    /// - Parameters:
    ///   - name: Name of caller (for logging)
    ///   - value: The value to pass to throttledEvent closur
    public func skipThrottle(name: String? = #function,
                             value: T) {
        //        print("ThrottleLatest: unthrottled")
        throttledEvent?(value)
    }
    
    /// Cancels any pending events
    public func cancel() {
        values.removeAll()
        timer?.invalidate()
        timer = nil
    }
}
