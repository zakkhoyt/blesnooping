//
//  UIColor+HexString.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/8/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit

extension String {
    internal var hexStringValue: UInt32 {
        let six: String = self.contains("#")
            ? String(self.prefix(7))
            : String(self.prefix(6))
        let scanner = Scanner(string: six)
        scanner.charactersToBeSkipped = CharacterSet(arrayLiteral: "#")
        var color: UInt64 = 0
        scanner.scanHexInt64(&color)
        return UInt32(color)
    }
}

extension UIColor {
    // MARK: AnyObject factory
    
    /// Create a UIColor with a hex string
    ///
    /// - parameter hexString: RRGGBB or #RRGGBB where RR, GG, and BB range from "00" to "FF"
    /// - parameter alpha:     Alpha value from 0.0 to 1.0
    ///
    /// - returns: UIColor instance
    internal class func colorWith(hexString: String, alpha: CGFloat = 1.0) -> UIColor {
        return UIColor(hexString: hexString, alpha: alpha)
    }
    
    // MARK: Init methods
    
    /// Create a UIColor with a hex string
    ///
    /// - parameter hexString: RRGGBB or #RRGGBB
    /// - parameter alpha:     An alpha value from 0.0 to 1.0
    ///
    /// - returns: UIColor instance
    internal convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let color = hexString.hexStringValue
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /// Returns a hex string value for the color (alpha omitted)
    internal var hexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb: Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format: "%06X", rgb).uppercased()
    }
}

extension UIColor {
    /// Returns a random UIColor.system*
    internal static var randomSystemColor: UIColor {
        let colors: [UIColor] = [
            .systemRed,
            .systemGreen,
            .systemBlue,
            .systemOrange,
            .systemYellow,
            .systemPink,
            .systemPurple,
            .systemTeal,
            .systemIndigo,
            .cyan,
            .systemGray
        ]
        return colors[Int.random(in: 0..<colors.count)]
    }
    
    internal static var random: UIColor {
        UIColor(hue: CGFloat.random(in: 0.0...1.0),
                saturation: 1,
                brightness: 1,
                alpha: 1)
    }
}
