//
//  TableViewReusable.swift
//  Nightlight
//
//  Created by Ron Lee on 2/27/19.
//  Copyright © 2019 Zakk Hoyt. All rights reserved.
//

import UIKit
/**
 This is to be implemented by a UITableViewCell subclass, as a convenience to setup reusability by the caller.
 */
public protocol TableViewReusable {
    /// The identifier used for registering and dequeuing.
    static var reuseIdentifier: String { get }

    /// Used to handle registering the cell to the tableView (nib/class/etc)
    static func register(to tableView: UITableView)
}

extension TableViewReusable where Self: UITableViewCell {
    public static func register(to tableView: UITableView) {
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: reuseIdentifier, bundle: bundle)
        tableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
    }
}

extension TableViewReusable where Self: UITableViewHeaderFooterView {
    public static func register(to tableView: UITableView) {
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
}
