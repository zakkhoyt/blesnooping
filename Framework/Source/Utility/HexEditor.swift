//
//  HexEditor.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/6/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import Foundation

public struct HexEditor {
    public enum TextEncoding: CaseIterable, CustomStringConvertible {
        case ascii
        case macRoman
        case isoLatin1
        case isoLatin2
        case utf16Little
        case utf16Big
        
        public var description: String {
            switch self {
            case .ascii: return "Ascii"
            case .macRoman: return "MacRoman"
            case .isoLatin1: return "ISO Latin 1 (Western Europe)"
            case .isoLatin2: return "ISO Latin 2 (Eastern Europe)"
            case .utf16Little: return "UTF-16 Little (0x0FFFE)"
            case .utf16Big: return "UTF-16 Big (0x0FEFF)"
            }
        }
        
        fileprivate var characters: String {
            // swiftlint:disable line_length
            switch self {
            case .ascii: return         ###"                                 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~................................................................................................................................."###
            case .macRoman: return      ###"                                 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ ÄÅÇÉÑÖÜáàâäãåçéèêëíìîïñóòôöõúùûü†°¢£§•¶ß®©™´¨≠ÆØ∞±≤≥¥µ∂∑∏π∫ªºΩæø¿¡¬√ƒ≈∆«»… ÀÃÕŒœ–—“”‘’÷◊ÿŸ⁄€‹›ﬁﬂ‡·‚„‰ÂÊÁËÈÍÎÏÌÓÔÒÚÛÙıˆ˜¯˘˙˚¸˝˛ˇ"###
            case .isoLatin1: return     ###"                                 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~                            ¡¢£¤¥¦§¨©ª«¬ ®¯°±²³´µ¶·¸¹º»¼½¾¿ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõö÷øùúûüýþÿ"###
            case .isoLatin2: return     ###"                                 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~                            Ą˘Ł¤ĽŚ§¨ŠŞŤŹ­ŽŻ°ą˛ł´ľśˇ¸šşťź˝žżŔÁÂĂÄĹĆÇČÉĘËĚÍÎĎĐŃŇÓÔŐÖ×ŘŮÚŰÜÝŢßŕáâăäĺćçčéęëěíîďđńňóôőö÷řůúűüýţ˙"###
            case .utf16Little: return   ###""###
            case .utf16Big: return      ###""###
            }
            // swiftlint:enable line_length
        }
    }

    public static func hexEditorString(data: Data, textEncoding: HexEditor.TextEncoding) -> String {
        let characters = textEncoding.characters
        switch textEncoding {
        case .ascii, .macRoman, .isoLatin1, .isoLatin2:
            let characterMap = generate8Bit(input: characters)
            return data.bytes.compactMap { characterMap[$0] }.joined()
        case .utf16Little:
            let characterMap = generateUnicode(input: characters, textEncoding: textEncoding)
            return data.littleEndianValues.compactMap { characterMap[$0] }.joined()
        case .utf16Big:
            let characterMap = generateUnicode(input: characters, textEncoding: textEncoding)
            return data.bigEndianValues.compactMap { characterMap[$0] }.joined()
        }
    }
    private static func generate8Bit(input hexeditor: String) -> [UInt8: String] {
        assert(hexeditor.count == 256, "Improper string length. Expected 256. Got \(hexeditor.count).")
        var hexCharMap: [UInt8: String] = [:]
        for i in UInt8(0x00)...UInt8(0xFF) {
            let index = hexeditor.index(hexeditor.startIndex, offsetBy: Int(i))
            hexCharMap[i] = String(hexeditor[index])
        }
        return hexCharMap
    }
    
    private static func generateUnicode(input hexeditor: String, textEncoding: HexEditor.TextEncoding) -> [UInt16: String] {
        var unicodeCharMap: [UInt16: String] = [:]
        for i in stride(from: UInt(0x00), to: UInt(0xFF), by: 0x02) {
            let value: UInt16
            switch textEncoding {
            case .utf16Little: value = UInt16((i + 1) * 256 + i)
            case .utf16Big: value = UInt16(i * 256 + (i + 1))
            default:
                assertionFailure("Can only pass .utf16Little or .utf16Big")
                value = 0
            }
            if let scalar = Unicode.Scalar(value) {
                let string = String(scalar)
                unicodeCharMap[value] = string
            } else {
                unicodeCharMap[value] = "."
            }
        }
        return unicodeCharMap
    }
}
