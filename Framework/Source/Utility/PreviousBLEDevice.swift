//
//  PreviousBLEDevice.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation

private let kNameKey = "name"
private let kUUIDKey = "uuid"
private let kPeripheralKey = "uuid"
private let kDeviceKey = "device"
private let kDateKey = "date"

/// Since CBPeripheral does not support NSCodable, we can't encode/decode it.
/// Instead we wil store the UUID and look for it in a scan.
public class PreviousBLEDevice: NSObject, NSSecureCoding {
    public static var supportsSecureCoding: Bool {
        return true
    }
    
    public let name: String?
    public let uuid: String

    public init(device: BLEDevice) {
        self.name = device.peripheral.description
        self.uuid = device.peripheral.identifier.uuidString
    }

    // MARK: NSCoding

    public required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: kNameKey) as? String

        guard let uuid = aDecoder.decodeObject(forKey: kUUIDKey) as? String else {
            return nil
        }
        self.uuid = uuid
    }

    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.name, forKey: kNameKey)
        aCoder.encode(self.uuid, forKey: kUUIDKey)
    }
}
