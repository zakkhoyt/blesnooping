//
//  Logger.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation

public class Logger {
    public enum Level: CustomStringConvertible {
        case info
        case debug
        case warning
        case error
        case critical
        
        public var description: String {
            switch self {
            case .info: return "info"
            case .debug: return "debug"
            case .warning: return "warning"
            case .error: return "error"
            case .critical: return "critical"
            }
        }
        
        public var levelString: String {
            let value: String = {
                switch self {
                case .info: return "info"
                case .debug: return "debug"
                case .warning: return "warning"
                case .error: return "error"
                case .critical: return "critical"
                }
            }()
            return "[\(value)]"
        }
    }
    
    public class func info(_ category: String? = nil,
                           _ message: String,
                           file: StaticString = #file,
                           function: StaticString = #function,
                           line: Int = #line) {
        log(.debug, category, message, file: file, function: function, line: line)
    }
    
    public class func debug(_ category: String? = nil,
                            _ message: String,
                            file: StaticString = #file,
                            function: StaticString = #function,
                            line: Int = #line) {
        log(.debug, category, message, file: file, function: function, line: line)
    }
    
    public class func warning(_ category: String? = nil,
                              _ message: String,
                              file: StaticString = #file,
                              function: StaticString = #function,
                              line: Int = #line) {
        log(.debug, category, message, file: file, function: function, line: line)
    }
    
    public class func error(_ category: String? = nil,
                            _ message: String,
                            file: StaticString = #file,
                            function: StaticString = #function,
                            line: Int = #line) {
        log(.debug, category, message, file: file, function: function, line: line)
    }
    
    public class func error(_ category: String? = nil,
                            _ error: Error,
                            file: StaticString = #file,
                            function: StaticString = #function,
                            line: Int = #line) {
        log(.debug, category, error.localizedDescription, file: file, function: function, line: line)
    }
    
    public class func error(_ error: Error,
                            file: StaticString = #file,
                            function: StaticString = #function,
                            line: Int = #line) {
        log(.debug, nil, error.localizedDescription, file: file, function: function, line: line)
    }
    
    public class func critical(_ category: String? = nil,
                               _ message: String,
                               file: StaticString = #file,
                               function: StaticString = #function,
                               line: Int = #line) {
        log(.debug, category, message, file: file, function: function, line: line)
    }
    
    public class func log(_ level: Level = .debug,
                          _ category: String? = nil,
                          _ message: String,
                          file: StaticString = #file,
                          function: StaticString = #function,
                          line: Int = #line) {
        let categoryString: String = {
            guard let category = category else { return "" }
            return "[-\(category)-] "
        }()
        
        let elapsedString = String(format: "%.3f", Date().timeIntervalSince1970)
        let fileName = String("\(file)".split(separator: "/").last ?? "")
        print("\(level.levelString) \(elapsedString): \(fileName):\(function):\(line) \(categoryString)\(message)")
    }
}
