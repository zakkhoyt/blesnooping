//
//  HexString.swift
//  Nightlight
//
//  Created by Zakk Hoyt on 8/26/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation

extension Bool {
    public var boolString: String {
        return self == true ? "true" : "false"
    }
}

extension FixedWidthInteger {
    public var percentString: String {
        let percent = Float(self) / Float(Self.max) * 100
        return String(format: "%.0f%%", percent)
    }
}

extension FixedWidthInteger {
    public var binaryString: String {
        let binaryString = String(self, radix: 2)
        let isNegative = binaryString.prefix(1) == "-"
        var toAdd: Int = MemoryLayout<Self>.size * 8 - binaryString.count
        if toAdd < 0 { toAdd = 0 }
        var padded = binaryString.replacingOccurrences(of: "-", with: "") // input may have - sign
        for _ in 0 ..< toAdd {
            padded = "0" + padded
        }
        let prefix = isNegative ? "-" : ""
        return "\(prefix)0b\(padded)"
    }
}

extension FixedWidthInteger {
    public var hexString: String {
        let hexString = String(self, radix: 16)
        var toAdd: Int = MemoryLayout<Self>.size * 2 - hexString.count
        var padded = hexString.replacingOccurrences(of: "-", with: "") // input may have - sign
        if toAdd < 0 { toAdd = 0 }
        for _ in 0 ..< toAdd {
            padded = "0" + padded
        }
        return padded.uppercased()
    }
}

extension Data {
    public var hexString: String {
        return map { String(format: "%02hhX", $0) }.joined()
    }

    public var binaryString: String {
        let table: [String.Element: String] = [
            "0": "0000",
            "1": "0001",
            "2": "0010",
            "3": "0011",
            "4": "0100",
            "5": "0101",
            "6": "0110",
            "7": "0111",
            "8": "1000",
            "9": "1001",
            "A": "1010",
            "B": "1011",
            "C": "1100",
            "D": "1101",
            "E": "1110",
            "F": "1111"
        ]
        //return map { String(format: "%08b", $0) }.joined()
        //return self.reduce
        return hexString.map { table[$0] ?? "" }.joined()
    }

//    public var binaryString: String {
//        let binaryString = String(self, radix: 2)
//        let isNegative = binaryString.prefix(1) == "-"
//        var toAdd: Int = MemoryLayout<Self>.size * 8 - binaryString.count
//        if toAdd < 0 { toAdd = 0 }
//        var padded = binaryString.replacingOccurrences(of: "-", with: "") // input may have - sign
//        for _ in 0 ..< toAdd {
//            padded = "0" + padded
//        }
//        let prefix = isNegative ? "-" : ""
//        return "\(prefix)0b\(padded)"
//    }
//
}

extension Data {
    func unpack<T: FixedWidthInteger>() -> T {
        let mdata = self
        return mdata.withUnsafeBytes { $0.load(as: T.self) }
    }

    func swapAndUnpack<T: FixedWidthInteger>() -> T {
        let mdata = self
        return mdata.withUnsafeBytes { $0.load(as: T.self).byteSwapped }
    }
}

extension Data {
    public static func fromIntString(_ text: String) -> Data? {
        var data = UInt64(text)?.data
        data?.unpad()
        return data
    }
    
    /// Useful for removing leading 0s if you have converted a number to data.
    /// EX: UInt64(0xFF). This will produce data[0] = 0xFF, but data[1...7] will be 0x00
    /// This method strips off those leading 0s
    public mutating func unpad() {
        var indexToRemove = count
        for i in (0..<count).reversed() {
            if bytes[i] == 0 {
                indexToRemove = i
            } else {
                break
            }
        }
        if indexToRemove < count {
            removeSubrange(indexToRemove...)
        }
    }
    
    public mutating func pad(size: Int) -> Bool {
        guard self.count <= size else { return false }
        for _ in 0..<(size - self.count) {
            append(0x00)
        }
        return true
    }
}

extension FixedWidthInteger {
    public var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<Self>.size)
    }
}
extension Data {
    public func unsignedValue<T: FixedWidthInteger>() -> T {
        let size = MemoryLayout<T>.size
        guard self.count == size else {
            preconditionFailure("Data.count must be <= size of self (\(size))")
        }
        let i32array = self.withUnsafeBytes { $0.load(as: T.self) }
        return i32array
    }
}

extension String {
    public var isHexString: Bool {
        let regex = "[A-Fa-f0-9]*"
        let testString = NSPredicate(format: "SELF MATCHES %@", regex)
        return testString.evaluate(with: self)
    }
    
    public var isDecimalString: Bool {
        let regex = "[0-9]*"
        let testString = NSPredicate(format: "SELF MATCHES %@", regex)
        return testString.evaluate(with: self)
    }
    
    public var isOctalString: Bool {
        let regex = "[0-7]*"
        let testString = NSPredicate(format: "SELF MATCHES %@", regex)
        return testString.evaluate(with: self)
    }
}

extension Data {
    /// Returns as an array [UInt8]
    public var bytes: [UInt8] {
        var bytes = [UInt8](repeating: 0, count: self.count/MemoryLayout<UInt8>.stride)
        _ = bytes.withUnsafeMutableBytes { self.copyBytes(to: $0) }
        return bytes
    }
    
    /// Returns as an array [UInt16]
    public var littleEndianValues: [UInt16] {
        var bytes = [UInt16](repeating: 0, count: self.count/MemoryLayout<UInt16>.stride)
        _ = bytes.withUnsafeMutableBytes { self.copyBytes(to: $0) }
        return bytes
    }
    
    /// Returns as an array [UInt16] with bytes swapped (big endian)
    public var bigEndianValues: [UInt16] {
        var values = littleEndianValues
        for (i, value) in values.enumerated() {
            values[Int(i)] = value.byteSwapped
        }
        return values
    }
}

extension String {
    /// Create `Data` from hexadecimal string representation
    /// This creates a `Data` object from hex string. Note, if the string has any spaces or non-hex characters (e.g. starts with '<' and with a '>'), those are ignored and only hex characters are processed.
    /// - returns: Data represented by this hexadecimal string.
    public var hexadecimal: Data? {
        var data = Data(capacity: count / 2)
        do {
            let regex = try NSRegularExpression(pattern: "[0-9a-f]{1,2}", options: .caseInsensitive)
            regex.enumerateMatches(in: self, range: NSRange(startIndex..., in: self)) { match, _, _ in
                let byteString = (self as NSString).substring(with: match!.range)
                let num = UInt8(byteString, radix: 16)!
                data.append(num)
            }
            
            guard data.count > 0 else { return nil }
            
            return data
        } catch {
            return nil
        }
    }
}
