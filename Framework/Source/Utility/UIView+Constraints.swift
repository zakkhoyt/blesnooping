//
//  UIView+Constraints.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit

extension UIView {
    /// Pins the view to edges of the parent's superview.
    ///
    /// - Parameters:
    ///   - edges: The edges to pin to the parent's superview.
    ///   - useLayoutMargins: Whether to use parent's layout margin guide or not.
    public func pinEdgesToParent(_ edges: UIRectEdge = .all,
                                 useSafeAreaLayoutGuide: Bool = true,
                                 insets: UIEdgeInsets = .zero) {
        guard let superview = superview else {
            assertionFailure("View must have a superview")
            return
        }
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if edges.contains(.left) {
            let superviewLeadingAnchor = useSafeAreaLayoutGuide ?
                superview.safeAreaLayoutGuide.leadingAnchor :
                superview.leadingAnchor
            leadingAnchor.constraint(equalTo: superviewLeadingAnchor, constant: insets.left).isActive = true
        }
        
        if edges.contains(.right) {
            let superviewTrailingAnchor = useSafeAreaLayoutGuide ?
                superview.safeAreaLayoutGuide.trailingAnchor :
                superview.trailingAnchor
            trailingAnchor.constraint(equalTo: superviewTrailingAnchor, constant: -insets.right).isActive = true
        }
        
        if edges.contains(.top) {
            let superviewTopAnchor = useSafeAreaLayoutGuide ?
                superview.safeAreaLayoutGuide.topAnchor :
                superview.topAnchor
            topAnchor.constraint(equalTo: superviewTopAnchor, constant: insets.top).isActive = true
        }
        
        if edges.contains(.bottom) {
            let superviewBottomAnchor = useSafeAreaLayoutGuide ?
                superview.safeAreaLayoutGuide.bottomAnchor :
                superview.bottomAnchor
            bottomAnchor.constraint(equalTo: superviewBottomAnchor, constant: -insets.bottom).isActive = true
        }
    }
}
