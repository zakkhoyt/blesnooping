//
//  BLEReconnectManager.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

public class BLEReconnectScanner: BLEScanner, BLEScannerDelegate {
    private lazy var scanner: BLEBasicScanner = BLEBasicScanner(delegate: self)
    
    public init(delegate: BLEScannerDelegate) {
        self.delegate = delegate
    }

    // MARK: Implements BLEScannerController
    
    // swiftlint:disable implicitly_unwrapped_optional
    public weak var delegate: BLEScannerDelegate!
    // swiftlint:enable implicitly_unwrapped_optional
    
    public var isScanning: Bool {
        scanner.isScanning
    }
    
    public func resetDevices() {
        scanner.resetDevices()
    }
    
    public func startScanning() {
        scanner.startScanning()
    }
    
    public func stopScanning() {
        scanner.stopScanning()
    }
    
    public func connect(to device: BLEDevice) {
        scanner.connect(to: device)
    }
    
    public func disconnect(from device: BLEDevice) {
        scanner.disconnect(from: device)
    }
    
    // MARK: Implements BLEScannerDelegate

    public func bleScanner(_ bleScanner: BLEScanner, didUpdateState state: CBManagerState) {
        delegate.bleScanner(self, didUpdateState: state)
    }

    public func bleScanner(_ bleScanner: BLEScanner, didUpdateDesiredScanState scanState: BLEBasicScanner.ScanState) {
        delegate.bleScanner(self, didUpdateDesiredScanState: scanState)
    }
    
    public func bleScanner(_ bleScanner: BLEScanner, didUpdateDevices devices: [BLEDevice]) {
        delegate.bleScanner(self, didUpdateDevices: devices)
        
        func checkForPreviousBLEDevice() {
            guard let previousDevice = PublicUserDefaults.readPreviousDevice() else { return }
            guard let deviceToReconnectTo = (devices.filter { $0.peripheral.identifier.uuidString == previousDevice.uuid }
                .first) else { return }
            guard let isConnectable = deviceToReconnectTo.isConnectable, isConnectable > 0 else { return }
            guard deviceToReconnectTo.peripheral.state == .disconnected else { return }
            Logger.debug("Reconnect", "Attempting to reconnect to: \(deviceToReconnectTo)")
            connect(to: deviceToReconnectTo)
        }
//        checkForPreviousBLEDevice()
    }
    
    public func bleScanner(_ bleScanner: BLEScanner, didConnectTo device: BLEDevice) {
        delegate.bleScanner(self, didConnectTo: device)
    }
    
    public func bleScanner(_ bleScanner: BLEScanner, didFailToConnectTo device: BLEDevice) {
        delegate.bleScanner(self, didFailToConnectTo: device)
    }
    
    public func bleScanner(_ bleScanner: BLEScanner, didDisconnectFrom device: BLEDevice, isExpected: Bool) {
        delegate.bleScanner(self, didDisconnectFrom: device, isExpected: isExpected)
    }
}
