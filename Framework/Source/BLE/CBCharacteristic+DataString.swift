//
//  CBCharacteristic+DataString.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/4/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

extension CBCharacteristic {
    public var valueString: String {
        guard let value = self.value else { return kUnavailable }
        if uuid.uuidString == BLEDevice.BLECharacteristic.battery {
            var u: UInt8 = 0
            value.copyBytes(to: &u, count: MemoryLayout.size(ofValue: u))
            return "0x\(u.hexString)"
        }
        
        if let string = String(data: value, encoding: String.Encoding.utf8) {
            return string.replacingOccurrences(of: "\0", with: "")
        }
        
        return "0x\(value.hexString)"
    }
}
