//
//  CBDescriptor+Helpers.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol DescriptorDescribable {
    var summaryString: String { get }
    var descriptorString: String { get }
    var uuidString: String { get }
    var valueSentString: String { get }
}

public let kUnavailable = "N/A"

extension CBDescriptor: DescriptorDescribable {
    public var summaryString: String {
        "\n" + [descriptorString, uuidString, valueString, valueSentString]
            //.compactMap { "\t\($0)" }
            .joined(separator: "\n")
    }
    
    public var descriptorString: String { "Descriptor: " + (CBDescriptor.nameMap[uuid.uuidString] ?? kUnavailable) }

    public var uuidString: String { "UUID: " + uuid.uuidString }

    public var valueString: String {
        let prefix = "Value: "
        guard let data = self.value as? Data else { return "\(prefix) \(kUnavailable)" }
                
        switch uuid.uuidString {
        case CBUUIDCharacteristicExtendedPropertiesString:
            guard let number = value as? NSNumber else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(number.floatValue)"
            
        case CBUUIDCharacteristicUserDescriptionString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"

        case CBUUIDClientCharacteristicConfigurationString:
            guard let number = value as? NSNumber else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(number.floatValue)"
            
        case CBUUIDServerCharacteristicConfigurationString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"
            
        case CBUUIDCharacteristicFormatString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"

        case CBUUIDCharacteristicAggregateFormatString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"

        case CBUUIDCharacteristicValidRangeString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"

        case CBUUIDL2CAPPSMCharacteristicString:
            guard let string = String(data: data, encoding: .utf8) else { return "\(prefix) \(kUnavailable)" }
            return "\(prefix) \(string)"

        default:
            return "\(prefix) \(kUnavailable)"
        }
    }
    
    public var valueSentString: String { return "ValueSent: \(kUnavailable)" }
}

extension CBDescriptor {
    // swiftlint:disable line_length
    static let nameMap: [String: String] = [
        CBUUIDCharacteristicExtendedPropertiesString: "Characteristic Extended Properties",
        CBUUIDCharacteristicUserDescriptionString: "Characteristic User Description",
        CBUUIDClientCharacteristicConfigurationString: "Client Characteristic Configuration",
        CBUUIDServerCharacteristicConfigurationString: "Server Characteristic Configuration",
        CBUUIDCharacteristicFormatString: "Characteristic Format",
        CBUUIDCharacteristicAggregateFormatString: "Characteristic Aggregate Format",
        CBUUIDCharacteristicValidRangeString: "Characteristic Valid Range",
        CBUUIDL2CAPPSMCharacteristicString: "L2CAP PSM Characteristic"
    ]
    
    static let descriptionMap: [String: String] = [
        CBUUIDCharacteristicExtendedPropertiesString: "The corresponding value for this descriptor is an NSNumber object",
        CBUUIDCharacteristicUserDescriptionString: "The corresponding value for this descriptor is an <code>NSString</code> object",
        CBUUIDClientCharacteristicConfigurationString: "The corresponding value for this descriptor is an <code>NSNumber</code> object",
        CBUUIDServerCharacteristicConfigurationString: "The corresponding value for this descriptor is an <code>NSNumber</code> object",
        CBUUIDCharacteristicFormatString: "The corresponding value for this descriptor is an <code>Data</code> object",
        CBUUIDCharacteristicAggregateFormatString: "The string representation of the UUID for the aggregate descriptor",
        CBUUIDCharacteristicValidRangeString: "Data representing the valid min/max values accepted for a characteristic",
        CBUUIDL2CAPPSMCharacteristicString: "The PSM (a little endian uint16_t) of an L2CAP Channel associated with the GATT service containing this characteristic. Servers can publish this characteristic with the UUID ABDD3056-28FA-441D-A470-55A75A52553A"
    ]
    // swiftlint:enable line_length
}
