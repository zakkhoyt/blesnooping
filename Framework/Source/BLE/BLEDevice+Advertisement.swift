//
//  BLEDevice+Manufacturer.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/6/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BLEDevice {
    /// The first two bytes of manufacturerData unpacked as a UInt16 then to hex string.
    public var advertisedManufacturerValue: UInt16? {
        guard let data = manufacturerData, data.count > 2 else { return nil }
        let manufacturerValue: UInt16 = data[0..<2].unpack()
        return manufacturerValue
    }
    
    // Everthing after the first two bytes of advertised manufacturer data.
    public var advertisedManufacturerDataPayload: Data? {
        guard let data = manufacturerData, data.count > 2 else { return nil }
        return data[2..<data.count]
    }

    public var advertisedManufacturerDataString: String? {
        // Manfacturer data is first two bytes of advertised manufacturer data.
        guard let data = manufacturerData, data.count > 2 else {
            return nil
        }
        guard let manufacturerValue = advertisedManufacturerValue else { return nil }
        let manufacturerPayload = self.advertisedManufacturerDataPayload ?? Data()
        let key = manufacturerValue.hexString
        guard let manufacturerName = BLEDevice.BLEManufacturer.nameDictionary[key] else { return "0x\(data.hexString)" }
        return "\(manufacturerName) <0x\(key)>\n0x\(manufacturerPayload.hexString)"
    }
}

extension BLEDevice {
    public var advertisedServicesString: String? {
        services?.map { cbuuid in
            var parts: [String] = []
            if let name = BLEDevice.BLEService.nameDictionary[cbuuid.uuidString] {
                parts.append(name)
            }
            parts.append("<0x\(cbuuid.uuidString)>")
            return parts.joined(separator: " ")
        }.joined(separator: "\n")
    }
}

extension BLEDevice {
    public var advertisedServicesDataString: String? {
        serviceData?
            .map { "\($0.key) 0x\($0.value.hexString)" }
            .joined(separator: "\n")
    }
}

extension BLEDevice {
    public var advertisedOverflowServiceUUIDsString: String? {
        overflowServiceUUIDs?
            .map { "<0x\($0.uuidString)>" }
            .joined(separator: ", ")
    }
}

extension BLEDevice {
    public var advertisedSolicitedServiceUUIDsString: String? {
        solicitedServiceUUIDs?
            .map { "<0x\($0.uuidString)>" }
            .joined(separator: ", ")
    }
}
