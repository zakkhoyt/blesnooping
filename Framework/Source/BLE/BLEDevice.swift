//
//  BLEDevice.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//
//  open func maximumWriteValueLength(for type: CBCharacteristicWriteType) -> Int
//      optional func peripheral(_ peripheral: CBPeripheral, didModifyServices invalidatedServices: [CBService])
//      optional func peripheral(_ peripheral: CBPeripheral, didDiscoverIncludedServicesFor service: CBService, error: Error?)

import Foundation
import UIKit
import CoreBluetooth

public protocol BLEDeviceDelegate: AnyObject {
    func bleDeviceDidInitialize(_ device: BLEDevice)
    func bleDevicedidUpdatePeripheralName(_ device: BLEDevice)
    func bleDevice(_ device: BLEDevice, didDiscoverServices services: [CBService])
    func bleDevice(_ device: BLEDevice, didDiscoverCharacteristics characteristics: [CBCharacteristic])
    func bleDevice(_ device: BLEDevice, didRead data: Data, fromCharacteristic characteristic: CBCharacteristic)
    func bleDevice(_ device: BLEDevice, didWrite data: Data, toCharacteristic characteristic: CBCharacteristic)
    
    func bleDevice(_ device: BLEDevice, didUpdateNotificationStateFor characteristic: CBCharacteristic)
    
    func bleDevice(_ device: BLEDevice, didDiscoverDescriptorsFor characteristic: CBCharacteristic)
}

public protocol BLEDeviceRSSIDelegate: AnyObject {
    func bleDevice(_ device: BLEDevice, didUpdateRSSIs rssis: [RSSITuple])
}

public protocol BLEDeviceDataSource: AnyObject {
    func bleDeviceScanner(_ bleDevice: BLEDevice) -> BLEBasicScanner
}

public typealias RSSITuple = (date: Date, rssi: Float)

public class BLEDevice: NSObject, CBPeripheralDelegate, AdvertisableItems {
    public var isFilteredOut: Bool = false
    public let peripheral: CBPeripheral
    
    public weak var rssiDelegate: BLEDeviceRSSIDelegate? {
        didSet {
            guard rssiDelegate != nil else {
                rssiThrottle.cancel()
                return
            }
            rssiThrottle.start(duration: 1.0) { [weak self] _ in
                guard let safeSelf = self else { return }
                safeSelf.rssiDelegate?.bleDevice(safeSelf, didUpdateRSSIs: safeSelf.rssis)
            }
        }
    }
    
    // swiftlint:disable implicitly_unwrapped_optional
    private weak var dataSource: BLEDeviceDataSource!
    // swiftlint:enable implicitly_unwrapped_optional
    
    //private(set) var advertisementDataCache = AdvertisementDataCache()
    public private(set) var advertisementData: [String: Any] = [:]
    public private(set) var rssis: [RSSITuple] = [] {
        didSet {
            rssiThrottle.throttle(value: 0)
        }
    }
    
    private var rssiThrottle = ThrottleLatest<Float>()
    public private(set) var rssi: Float = 0
    public private(set) var rssiDate: Date = Date()
    public var elapsedSinceRssi: String {
        let elapsed = Date().timeIntervalSince(rssiDate)
        if elapsed < 1 {
            return String(format: "%.3fms", elapsed * 1000)
        } else {
            return String(format: "%.1fs", elapsed)
        }
    }
    public private(set) var delegate: BLEDeviceDelegate?
    private(set) var ioManager = IOManager()
    
    let advertisedItems: AdvertisedItems
    
    public private(set) var isInitialized: Bool = false
    public private(set) var initializedDate: Date?
    
    public let createdDate = Date()
    public private(set) var updateDate = Date()
    
    public let color = UIColor.random
    
    public var scanner: BLEBasicScanner {
        dataSource.bleDeviceScanner(self)
    }
    
    public var name: String? {
        if let name = peripheral.name {
            return name
        } else if let advertisedName = self.advertisedItems.localName {
            return advertisedName
        } else {
            return nil
        }
    }
    
    public var expectingDisconnect: Bool = false
    
    public init(peripheral: CBPeripheral, dataSource: BLEDeviceDataSource) {
        self.peripheral = peripheral
        self.dataSource = dataSource
        self.advertisedItems = AdvertisedItems(uuidString: peripheral.identifier.uuidString)
        super.init()
    }
    
    public override var hash: Int {
        updateDate.hashValue
    }
    
    public override func isEqual(_ object: Any?) -> Bool {
        guard let otherDevice = object as? BLEDevice else { return false }
        return otherDevice.peripheral == peripheral
    }
    
    public override var description: String {
        if let name = peripheral.name {
            return name
        } else if let advertisedName = self.advertisedItems.localName {
            return advertisedName
        } else {
            return peripheral.identifier.uuidString
        }
    }
    
    public override var debugDescription: String {
        var outputs: [String] = []
        if let name = peripheral.name {
            outputs.append(name)
        }
        if let advertisedName = self.advertisedItems.localName {
            outputs.append(advertisedName)
        }
        outputs.append(peripheral.identifier.uuidString)
        return outputs.joined(separator: ", ")
    }
    
    public func append(advertisementData adPart: [String: Any],
                       rssi: NSNumber) {
        updateDate = Date()
        adPart.forEach { arg0 in
            let (key, value) = arg0
            self.advertisementData[key] = value
            self.rssis.append((Date(), rssi.floatValue))
            self.rssi = rssi.floatValue
            self.rssiDate = Date()
        }
        
        parseAdvertisementData()
    }
    
    public func initialize(delegate: BLEDeviceDelegate) {
        self.delegate = delegate
        reset()
        peripheral.delegate = self
        
        Logger.debug("BLE Peripheral", "Requesting discovery of services for peripheral: \(peripheral)")
        peripheral.discoverServices(nil)
        setInitializeComplete()
    }
    
    public func reset() {
        self.ioManager = IOManager()
        isInitialized = false
        initializedDate = nil
    }
    
    private func parseAdvertisementData() {
        func parseAdvertisedName() {
            if let advertisedName = advertisementData[CBAdvertisementDataLocalNameKey] as? String {
                Logger.debug("Yep", advertisedName)
                self.advertisedItems.localName = advertisedName
            } else if let d = advertisementData[CBAdvertisementDataLocalNameKey] {
                print("inspect d: \(String(describing: d))")
            }
        }
        parseAdvertisedName()
        
        func parseTxPowerLevel() {
            guard let advertisedTXPowerLevel = advertisementData[CBAdvertisementDataTxPowerLevelKey] as? NSNumber else { return }
            self.advertisedItems.txPowerLevel = advertisedTXPowerLevel.floatValue
        }
        parseTxPowerLevel()
        
        func parseService() {
            guard let advertisedServices = advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID] else { return }
            self.advertisedItems.services = advertisedServices
        }
        parseService()
        
        func parseServiceData() {
            guard let advertisedServiceData = advertisementData[CBAdvertisementDataServiceDataKey] as? [CBUUID: Data] else { return }
            self.advertisedItems.serviceData = advertisedServiceData
        }
        parseServiceData()
        
        // https://www.bluetooth.com/specifications/assigned-numbers/company-identifiers/
        func parseManufacturerData() {
            guard let advertisedManufacturerData = advertisementData[CBAdvertisementDataManufacturerDataKey] as? Data else { return }
            self.advertisedItems.manufacturerData = advertisedManufacturerData
        }
        parseManufacturerData()
        
        func parseOverflowServiceUUIDs() {
            guard let advertisedOverflowServiceUUIDs = advertisementData[CBAdvertisementDataOverflowServiceUUIDsKey] as? [CBUUID] else { return }
            self.advertisedItems.overflowServiceUUIDs = advertisedOverflowServiceUUIDs
        }
        parseOverflowServiceUUIDs()
        
        func parseAdversisedConnectable() {
            if let advertisedIsConnectable = advertisementData[CBAdvertisementDataIsConnectable] as? NSNumber {
                self.advertisedItems.isConnectable = advertisedIsConnectable.intValue
            }
        }
        parseAdversisedConnectable()
        
        func parseSolicitedServiceUUIDs() {
            guard let advertisedSolicitedServiceUUIDs = advertisementData[CBAdvertisementDataSolicitedServiceUUIDsKey] as? [CBUUID] else { return }
            self.advertisedItems.solicitedServiceUUIDs = advertisedSolicitedServiceUUIDs
        }
        parseSolicitedServiceUUIDs()
    }
    
    private func setInitializeComplete() {
        guard !isInitialized else { return }
        isInitialized = true
        initializedDate = Date()
        delegate?.bleDeviceDidInitialize(self)
    }
    
    public func read(characteristic: CBCharacteristic) {
        let name = BLECharacteristic.nameDictionary[characteristic.uuid.uuidString] ?? ""
        Logger.debug("BLE Characteristic", "Requesting read for characteristic: \(name)")
        peripheral.readValue(for: characteristic)
    }
    
    // MARK: Implements CBPerihperalDelegate
    
    public func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
        Logger.debug("BLE Service", "Did update name for device: \(self)")
        // TODO: ??
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        if let error = error {
            Logger.debug("BLE Service", "Did encounter error while discovering services: \(error)")
            return
        }
        
        //peripheral.services?.forEach { [weak self]
        guard let services = peripheral.services else { return }
        for service in services {
            if let name = BLEDevice.BLEService.nameDictionary[service.uuid.uuidString] {
                Logger.debug("BLE Services", "Did discover \(name) service")
            } else {
                Logger.debug("BLE Services", "Did discover unknown service")
            }
            
            ioManager.discovered(service: service)
            //            checkIfAllSericesAndCharacteristicsDiscovered()
            
            Logger.debug("BLE Descriptor", "Requesting discovery of characteristics for service: \(service)")
            peripheral.discoverCharacteristics(nil, for: service)
        }
        delegate?.bleDevice(self, didDiscoverServices: services)
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didDiscoverCharacteristicsFor service: CBService,
                           error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while discovering characteristics: \(error)")
            return
        }
        
        guard let characteristics = service.characteristics else { return }
        for characteristic in characteristics {
            if let name = BLEDevice.BLECharacteristic.nameDictionary[characteristic.uuid.uuidString] {
                Logger.debug("BLE Services", "Did discover \(name) characteristic")
            } else {
                Logger.debug("BLE Services", "Did discover unknown characteristic")
            }
            
            //            ioManager.discovered(characteristic: characteristic)
            //            checkIfAllSericesAndCharacteristicsDiscovered()
            
            Logger.debug("BLE Descriptor", "Requesting discovery of descriptors for characteristic: \(characteristic)")
            peripheral.discoverDescriptors(for: characteristic)
        }
        
        delegate?.bleDevice(self, didDiscoverCharacteristics: characteristics)
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didUpdateValueFor characteristic: CBCharacteristic,
                           error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while reqeusting a read from characteristic: \(error)")
            return
        }
        
        guard let data = characteristic.value else {
            Logger.debug("BLE Characteristic", "Characteristic did update value but there is no data to read")
            return
        }
        
        if let name = BLEDevice.BLECharacteristic.nameDictionary[characteristic.uuid.uuidString] {
            Logger.debug("BLE Services", "Did read value from characteristic. \(name): \(characteristic.valueString)")
        } else {
            Logger.debug("BLE Services", "Did read value from characteristic: \(characteristic.uuid.uuidString): \(characteristic.valueString)")
        }
        //        ioManager.didRead(characteristic: characteristic, string: dataString)
        delegate?.bleDevice(self, didRead: data, fromCharacteristic: characteristic)
        //        checkIfAllCharacteristicsRead()
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didUpdateNotificationStateFor characteristic: CBCharacteristic,
                           error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while updating notification state for characteristic: \(characteristic)\n\(error)")
            return
        }
        
        Logger.debug("BLE Characteristic", "Did update notification state of characteristic \(characteristic) to \(characteristic.isNotifying.boolString)")
        delegate?.bleDevice(self, didUpdateNotificationStateFor: characteristic)
    }
    
    public func peripheral(_ peripheral: CBPeripheral,
                           didReadRSSI rssi: NSNumber,
                           error: Error?) {
        if let error = error {
            Logger.debug("BLE Device", "Did encounter error while reading rssi for device: \(description)\n\(error)")
            return
        }
        
        self.rssi = rssi.floatValue
    }
    
    // MARK: Descriptors:
    public func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while discovering descriptors for characteristic: \(characteristic)\n\(error)")
            return
        }
        
        Logger.debug("BLE Descriptor", "Did discover descriptors for characteristic: \(characteristic)")
        delegate?.bleDevice(self, didDiscoverDescriptorsFor: characteristic)
        
        guard let descriptors = characteristic.descriptors else { return }
        descriptors.forEach {
            Logger.debug("BLE Descriptor", "Requesting read for descriptor: \($0)")
            peripheral.readValue(for: $0)
        }
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor descriptor: CBDescriptor, error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while receiving update descriptor for descriptor: \(descriptor)\n\(error)")
            return
        }
        
        //        let CBUUIDCharacteristicExtendedPropertiesString: String
        //        The UUID for the Extended Properties descriptor, as a string.
        //        let CBUUIDCharacteristicUserDescriptionString: String
        //        The UUID for the User Description descriptor, as a string.
        //        let CBUUIDClientCharacteristicConfigurationString: String
        //        The UUID for the Client Configuration descriptor, as a string.
        //        let CBUUIDServerCharacteristicConfigurationString: String
        //        The UUID for the Server Configuration descriptor, as a string.
        //        let CBUUIDCharacteristicFormatString: String
        //        The UUID for the Presentation Format descriptor, as a string.
        //        let CBUUIDCharacteristicAggregateFormatString: String
        //        The UUID for the Aggregate Format descriptor, as a string.
        //        let CBUUIDL2CAPPSMCharacteristicString: String
        //        The PSM of an L2CAP channel associated with the GATT service containing this characteristic.
        
        guard let data = descriptor.value as? Data else { return }
        
        Logger.debug("BLE Descriptor", "Did update value for descriptor: \(descriptor)")
        
        switch descriptor.uuid.uuidString {
        case CBUUIDCharacteristicExtendedPropertiesString:
            Logger.debug("BLE Descriptor", "Extended properties: \(descriptor.uuid.uuidString)")
            guard let properties = descriptor.value as? NSNumber else {
                break
            }
            Logger.debug("BLE Descriptor", "Extended properties value: \(properties)")
        case CBUUIDCharacteristicUserDescriptionString:
            Logger.debug("BLE Descriptor", "User description: \(descriptor.uuid.uuidString)")
            guard let formatString = String(data: data, encoding: .utf8) else { return }
            Logger.debug("BLE Descriptor", "User description value: \(formatString)")
            
        case CBUUIDClientCharacteristicConfigurationString:
            Logger.debug("BLE Descriptor", "Client configuration: \(descriptor.uuid.uuidString)")
            guard let clientConfig = descriptor.value as? NSNumber else {
                break
            }
            Logger.debug("BLE Descriptor", "Client configuration value: \(clientConfig)")
        case CBUUIDServerCharacteristicConfigurationString:
            Logger.debug("BLE Descriptor", "Server configuration: \(descriptor.uuid.uuidString)")
            guard let serverConfig = descriptor.value as? NSNumber else {
                break
            }
            Logger.debug("BLE Descriptor", "Server configuration value: \(serverConfig)")
        case CBUUIDCharacteristicFormatString:
            Logger.debug("BLE Descriptor", "Format: \(descriptor.uuid.uuidString)")
            guard let format = descriptor.value as? NSData else {
                break
            }
            Logger.debug("BLE Descriptor", "Format value: \(format)")
        case CBUUIDCharacteristicAggregateFormatString:
            Logger.debug("BLE Descriptor", "Aggregate Format: \(descriptor.uuid.uuidString)")
            guard let formatString = String(data: data, encoding: .utf8) else { return }
            Logger.debug("BLE Descriptor", "Aggregate Format value: \(formatString)")
        default:
            Logger.debug("BLE Descriptor", "unknown descriptor uuid: \(descriptor.uuid.uuidString)")
        }
        
        //        if let data = descriptor.value as? Data {
        //            Logger.debug("BLE Descriptor", "Decoded as data: \(data)")
        //        } else if let dictionary = descriptor.value as? NSDictionary {
        //            Logger.debug("BLE Descriptor", "Decoded as dictionary: \(dictionary)")
        //        }
        //        Logger.debug("BLE Descriptor", "Did update value for descriptor: \(dictionary.description)")
    }
    
    public func peripheral(_ peripheral: CBPeripheral, didWriteValueFor descriptor: CBDescriptor, error: Error?) {
        if let error = error {
            Logger.debug("BLE Characteristic", "Did encounter error while updating writing descriptor state for descriptor: \(descriptor)\n\(error)")
            return
        }
    }
}

extension AdvertisableItems where Self: BLEDevice {
    public var localName: String? { advertisedItems.localName }
    public var txPowerLevel: Float? { advertisedItems.txPowerLevel }
    public var services: [CBUUID]? { advertisedItems.services }
    public var serviceData: [CBUUID: Data]? { advertisedItems.serviceData }
    public var manufacturerData: Data? { advertisedItems.manufacturerData }
    public var overflowServiceUUIDs: [CBUUID]? { advertisedItems.overflowServiceUUIDs }
    public var isConnectable: Int? { advertisedItems.isConnectable }
    public var solicitedServiceUUIDs: [CBUUID]? { advertisedItems.solicitedServiceUUIDs }
}
