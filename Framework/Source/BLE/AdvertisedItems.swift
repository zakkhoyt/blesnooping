//
//  AdvertisedItems.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

public protocol AdvertisableItems {
    var localName: String? { get }
    var txPowerLevel: Float? { get }
    var services: [CBUUID]? { get }
    var serviceData: [CBUUID: Data]? { get }
    var manufacturerData: Data? { get }
    var overflowServiceUUIDs: [CBUUID]? { get }
    var isConnectable: Int? { get }
    var solicitedServiceUUIDs: [CBUUID]? { get }
}

public class AdvertisedItems: AdvertisableItems {
    public var localName: String?
    public var txPowerLevel: Float?
    public var services: [CBUUID]?
    public var serviceData: [CBUUID: Data]?
    public var manufacturerData: Data?
    public var overflowServiceUUIDs: [CBUUID]?
    public var isConnectable: Int?
    public var solicitedServiceUUIDs: [CBUUID]?
    
    private let uuidString: String
    
    init(uuidString: String) {
        self.uuidString = uuidString
    }
}
