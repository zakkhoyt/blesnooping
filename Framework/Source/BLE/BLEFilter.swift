//
//  BLEScannerFilter.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation

public struct BLEFilter: Codable {
    public struct Details<T: Codable>: Codable {
        public var isEnabled: Bool
        public var compareTo: T {
            didSet {
                update()
            }
        }
        
        private mutating func update() {
            switch compareTo {
            case (let text as String):
                isEnabled = text.count > 0
            default:
                break
            }
        }
    }

    public var name = Details<String>(isEnabled: false, compareTo: "") {
        didSet {
           updateIsEnabled()
        }
    }
    public var data = Details<String>(isEnabled: false, compareTo: "") {
        didSet {
           updateIsEnabled()
        }
    }
    public var rssiGreaterThan = Details<Float>(isEnabled: false, compareTo: -99) {
        didSet {
            updateIsEnabled()
        }
    }
    
    public var isEnabled: Bool = false

    public init() {
    }
    
    // MARK: Private functions
    
    private mutating func updateIsEnabled() {
//        if name.isEnabled || data.isEnabled || rssiGreaterThan.isEnabled {
//            isEnabled = true
//        }
    }
}

extension BLEFilter {
    public func shouldFilterOut(device: BLEDevice) -> Bool {
        guard isEnabled else { return false }
        
        var filteredOutByName: Bool {
            guard name.isEnabled else { return false }

            if let localName = device.localName {
                if localName.contains(name.compareTo) {
                    return false
                }
            }
            
            if let peripheralName = device.peripheral.name {
                if peripheralName.contains(name.compareTo) {
                    return false
                }
            }
            return true
        }
        
        var filteredOutByData: Bool {
            guard data.isEnabled else { return false }

            // The first two bytes of manufacturerData unpacked as a UInt16 then to hex string.
            if let advertisedData = device.advertisedManufacturerValue {
                if advertisedData.hexString.contains(data.compareTo) {
                    return false
                }
            }
            
            // Search through manufacturerData payload [2..count-1] as a hexString
            if let advertisedManufacturerDataPayloadString = device.advertisedManufacturerDataPayload?.hexString {
                if advertisedManufacturerDataPayloadString.contains(data.compareTo) {
                    return false
                }
            }
            
            if let serviceData = device.serviceData {
                for (key, value) in serviceData {
                    if key.uuidString.contains(data.compareTo) {
                        return false
                    }
                    if value.hexString.contains(data.compareTo) {
                        return false
                    }
                }
            }
            if let services = device.services {
                for service in services {
                    if service.uuidString.contains(data.compareTo) {
                        return false
                    }
                }
            }
            return true
        }
        
        var filteredOutByRSSI: Bool {
            guard rssiGreaterThan.isEnabled else { return false }
            return device.rssi < rssiGreaterThan.compareTo
        }
        
        return filteredOutByName || filteredOutByData || filteredOutByRSSI
    }
}
