//
//  BLEScanner.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//
// public convenience init(delegate: CBCentralManagerDelegate?, queue: DispatchQueue?)
// open func retrievePeripherals(withIdentifiers identifiers: [UUID]) -> [CBPeripheral]
// open func retrieveConnectedPeripherals(withServices serviceUUIDs: [CBUUID]) -> [CBPeripheral]

import Foundation
import CoreBluetooth

public protocol BLEScanner: AnyObject {
    // swiftlint:disable implicitly_unwrapped_optional
    var delegate: BLEScannerDelegate! { get }
    // swiftlint:enable implicitly_unwrapped_optional
    var isScanning: Bool { get }

    func resetDevices()
    func startScanning()
    func stopScanning()
    func connect(to device: BLEDevice)
    func disconnect(from device: BLEDevice)
}

public protocol BLEScannerDelegate: AnyObject {
    func bleScanner(_ bleScanner: BLEScanner, didUpdateState state: CBManagerState)
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDesiredScanState scanState: BLEBasicScanner.ScanState)
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDevices devices: [BLEDevice])
    func bleScanner(_ bleScanner: BLEScanner, didConnectTo device: BLEDevice)
    func bleScanner(_ bleScanner: BLEScanner, didFailToConnectTo device: BLEDevice)
    func bleScanner(_ bleScanner: BLEScanner, didDisconnectFrom device: BLEDevice, isExpected: Bool)
}

extension Notification.Name {
    public static let BLEScannerDidConnectToDevice = Notification.Name("BLEScannerDidConnectToDevice")
    public static let BLEScannerDidDisconnectFromDevice = Notification.Name("BLEScannerDidDisconnectFromDevice")
    
    public static let BLEScannerUnexpectedDisconnect = Notification.Name("BLEScannerUnexpectedDisconnect")
}

public class BLEBasicScanner: NSObject, BLEScanner, CBCentralManagerDelegate {
    public enum ScanState: Int, CustomStringConvertible, CaseIterable {
        case stopped
        case scanning
        
        public var description: String {
            switch self {
            case .stopped: return "Stopped"
            case .scanning: return "Scanning"
            }
        }
    }
    
    private var throttle = ThrottleLatest<Float>()
    
    private(set) var desiredScanState: ScanState = .stopped {
        didSet {
            switch desiredScanState {
            case .stopped: centralManager.stopScan()
            case .scanning: centralManager.scanForPeripherals(withServices: nil, options: nil)
            }
            delegate.bleScanner(self, didUpdateDesiredScanState: desiredScanState)
        }
    }
    
    public var isScanning: Bool {
        centralManager.isScanning
    }
    
    //    @Atomic(wrappedValue: [], queue: DispatchQueue(label: "BLEScanner.queue"))
    //    private(set) var devices: Set<BLEDevice>
    
    @Atomic(wrappedValue: [], queue: DispatchQueue(label: "BLEScanner.queue")) public private(set) var devices: [BLEDevice]
    
    lazy private(set) var centralManager: CBCentralManager = {
        let centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options: [:])
        return centralManager
    }()
    
    // swiftlint:disable implicitly_unwrapped_optional
    public private(set) weak var delegate: BLEScannerDelegate!
    // swiftlint:enable implicitly_unwrapped_optional

    public init(delegate: BLEScannerDelegate) {
        super.init()
        self.delegate = delegate
        throttle.start(duration: 1.0) { [weak self] _ in
            guard let safeSelf = self else { return }
            safeSelf.delegate.bleScanner(safeSelf, didUpdateDevices: safeSelf.devices)
        }
    }

    public func resetDevices() {
        devices.removeAll { $0.peripheral.state != .connected }
    }
    
    public func startScanning() {
        // 1.b) We are in charge of caching devices rather than CoreBluetooth. Faster responses, and we get several callbacks per device. Less efficient
        let options: [String: Any] = [CBCentralManagerScanOptionAllowDuplicatesKey: true]
        centralManager.scanForPeripherals(withServices: nil, options: options)
        desiredScanState = .scanning
        
        DispatchQueue.global().asyncAfter(deadline: .now() + 15) {
            self.stopScanning()
        }
    }
    
    public func stopScanning() {
        centralManager.stopScan()
        desiredScanState = .stopped
    }
    
    public func connect(to device: BLEDevice) {
        Logger.debug("BLE Connect", "attempting to connect to device: \(device)")
        centralManager.connect(device.peripheral, options: nil)
    }
    
    public func disconnect(from device: BLEDevice) {
        Logger.debug("BLE Connect", "attempting to disconnect from device: \(device)")
        device.expectingDisconnect = true
        centralManager.cancelPeripheralConnection(device.peripheral)
    }
    
    // MARK: Implements CBCentralManagerDelegate scanning stuff
    
    public func centralManagerDidUpdateState(_ central: CBCentralManager) {
        Logger.debug("BLE Scan", "desiredScanState: \(desiredScanState) centralManager.state: \(central.state)")
        switch (desiredScanState, central.state) {
        case (.scanning, .poweredOn):
            Logger.debug("BLE Scan", "calling startScanning()")
            startScanning()
        default:
            Logger.debug("BLE Scan", "calling stopScanning()")
            stopScanning()
        }
        
        delegate.bleScanner(self, didUpdateState: central.state)
    }
    
    public func centralManager(_ central: CBCentralManager,
                               didDiscover peripheral: CBPeripheral,
                               advertisementData: [String: Any],
                               rssi: NSNumber) {
        defer {
            throttle.throttle(value: 0)
        }
        
        let device: BLEDevice
        if let index = devices.firstIndex(where: { $0.peripheral == peripheral }) {
            device = devices[index]
        } else {
            device = BLEDevice(peripheral: peripheral, dataSource: self)
        }
        device.append(advertisementData: advertisementData, rssi: rssi)
        
        //        let device: BLEDevice = devices.first { $0.peripheral == peripheral } ?? BLEDevice(peripheral: peripheral, dataSource: self)
        //        device.append(advertisementData: advertisementData, rssi: rssi)
        
//        if let filter = self.filter {
//            guard !filter.bleScanner(self, shouldFilterOutDevice: device) else {
//                // We are ignoring this device
//                return
//            }
//        }
        
        if let index = devices.firstIndex(where: { $0.peripheral == peripheral }) {
//            Logger.debug("BLE Scan", "didDiscoverPeripheral: \(device). Updating")
            print("Updating. didDiscoverPeripheral: \(device)")
            devices[index] = device
        } else {
//            Logger.debug("BLE Scan", "didDiscoverPeripheral: \(device). Inserting")
            devices.append(device)
            print("Inserting. didDiscoverPeripheral: \(device)")
        }
    }
    
    // MARK: Implements CBCentralManagerDelegate connecting stuff
    
    public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        guard let device = devices.device(for: peripheral) else { return }
        Logger.debug("BLE Connect", "did connect to device: \(device)")
        delegate.bleScanner(self, didConnectTo: device)
        
        NotificationCenter.default.post(name: .BLEScannerDidConnectToDevice, object: device)
    }
    
    public func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        guard let device = devices.device(for: peripheral) else { return }
        Logger.debug("BLE Connect", "did fail to connect to device: \(device)")
        delegate.bleScanner(self, didFailToConnectTo: device)
    }
    
    public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        guard let device = devices.device(for: peripheral) else { return }
        Logger.debug("BLE Connect", "did disconnect from device: \(device)")
        device.reset()
        delegate.bleScanner(self, didDisconnectFrom: device, isExpected: device.expectingDisconnect)
        if !device.expectingDisconnect {
            NotificationCenter.default.post(name: .BLEScannerUnexpectedDisconnect, object: device, userInfo: ["device": device])
        }
        NotificationCenter.default.post(name: .BLEScannerDidDisconnectFromDevice, object: device)
    }
}

extension BLEBasicScanner: BLEDeviceDataSource {
    public func bleDeviceScanner(_ bleDevice: BLEDevice) -> BLEBasicScanner {
        return self
    }
}
