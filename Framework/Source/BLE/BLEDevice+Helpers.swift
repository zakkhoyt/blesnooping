//
//  BLEDevice+Helpers.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import Foundation
import CoreBluetooth

extension BLEDevice {
    public struct BLEService {
        public static let genericAccess = "1800"
        public static let alertNotificationService = "1811"
        public static let automationIO = "1815"
        public static let batteryService = "180F"
        public static let binarySensor = "183B"
        public static let bloodPressure = "1810"
        public static let bodyComposition = "181B"
        public static let bondManagementService = "181E"
        public static let continuousGlucoseMonitoring = "181F"
        public static let currentTimeService = "1805"
        public static let cyclingPower = "1818"
        public static let cyclingSpeedAndCadence = "1816"
        public static let deviceInformation = "180A"
        public static let emergencyConfiguration = "183C"
        public static let environmentalSensing = "181A"
        public static let fitnessMachine = "1826"
        public static let genericAttribute = "1801"
        public static let glucose = "1808"
        public static let healthThermometer = "1809"
        public static let heartRate = "180D"
        public static let httpProxy = "1823"
        public static let humanInterfaceDevice = "1812"
        public static let immediateAlert = "1802"
        public static let indoorPositioning = "1821"
        public static let insulinDelivery = "183A"
        public static let internetProtocolSupportService = "1820"
        public static let linkLoss = "1803"
        public static let locationAndNavigation = "1819"
        public static let meshProvisioningService = "1827"
        public static let meshProxyService = "1828"
        public static let nextDSTChangeService = "1807"
        public static let objectTransferService = "1825"
        public static let phoneAlertStatusService = "180E"
        public static let pulseOximeterService = "1822"
        public static let reconnectionConfiguration = "1829"
        public static let referenceTimeUpdateService = "1806"
        public static let runningSpeedAndCadence = "1814"
        public static let scanParameters = "1813"
        public static let transportDiscovery = "1824"
        public static let txPower = "1804"
        public static let userData = "181C"
        public static let weightScale = "181D"

        public static let serviceUUIDs: [CBUUID] = {
            var output: [CBUUID] = []
            output.append(CBUUID(string: BLEService.deviceInformation))
            output.append(CBUUID(string: BLEService.batteryService))
            return output
        }()
        
        public static let all: [String] = [deviceInformation, batteryService]
        
        public static let nameDictionary: [String: String] = [
            genericAccess: "Generic Access",
            alertNotificationService: "Alert Notification Service",
            automationIO: "Automation IO",
            batteryService: "Battery Service",
            binarySensor: "Binary Sensor",
            bloodPressure: "Blood Pressure",
            bodyComposition: "Body Composition",
            bondManagementService: "Bond Management Service",
            continuousGlucoseMonitoring: "Continuous Glucose Monitoring",
            currentTimeService: "Current Time Service",
            cyclingPower: "Cycling Power",
            cyclingSpeedAndCadence: "Cycling Speed and Cadence",
            deviceInformation: "Device Information",
            emergencyConfiguration: "Emergency Configuration",
            environmentalSensing: "Environmental Sensing",
            fitnessMachine: "Fitness Machine",
            genericAttribute: "Generic Attribute",
            glucose: "Glucose",
            healthThermometer: "Health Thermometer",
            heartRate: "Heart Rate",
            httpProxy: "HTTP Proxy",
            humanInterfaceDevice: "Human Interface Device",
            immediateAlert: "Immediate Alert",
            indoorPositioning: "Indoor Positioning",
            insulinDelivery: "Insulin Delivery",
            internetProtocolSupportService: "Internet Protocol Support Service",
            linkLoss: "Link Loss",
            locationAndNavigation: "Location and Navigation",
            meshProvisioningService: "Mesh Provisioning Service",
            meshProxyService: "Mesh Proxy Service",
            nextDSTChangeService: "Next DST Change Service",
            objectTransferService: "Object Transfer Service",
            phoneAlertStatusService: "Phone Alert Status Service",
            pulseOximeterService: "Pulse Oximeter Service",
            reconnectionConfiguration: "Reconnection Configuration",
            referenceTimeUpdateService: "Reference Time Update Service",
            runningSpeedAndCadence: "Running Speed and Cadence",
            scanParameters: "Scan Parameters",
            transportDiscovery: "Transport Discovery",
            txPower: "Tx Power",
            userData: "User Data",
            weightScale: "Weight Scale"
        ]
    }
    
    public struct BLECharacteristic {
        public static let aerobicHeartRateLowerLimit = "2A7E"
        public static let aerobicHeartRateUpperLimit = "2A84"
        public static let aerobicThreshold = "2A7F"
        public static let age = "2A80"
        public static let aggregate = "2A5A"
        public static let alertCategoryID = "2A43"
        public static let alertCategoryIDBitMask = "2A42"
        public static let alertLevel = "2A06"
        public static let alertNotificationControlPoint = "2A44"
        public static let alertStatus = "2A3F"
        public static let altitude = "2AB3"
        public static let anaerobicHeartRateLowerLimit = "2A81"
        public static let anaerobicHeartRateUpperLimit = "2A82"
        public static let anaerobicThreshold = "2A83"
        public static let analog = "2A58"
        public static let analogOutput = "2A59"
        public static let apparentWindDirection = "2A73"
        public static let apparentWindSpeed = "2A72"
        public static let appearance = "2A01"
        public static let barometricPressureTrend = "2AA3"
        public static let batteryLevel = "2A19"
        public static let batteryLevelState = "2A1B"
        public static let batteryPowerState = "2A1A"
        public static let bloodPressureFeature = "2A49"
        public static let bloodPressureMeasurement = "2A35"
        public static let bodyCompositionFeature = "2A9B"
        public static let bodyCompositionMeasurement = "2A9C"
        public static let bodySensorLocation = "2A38"
        public static let bondManagementControlPoint = "2AA4"
        public static let bondManagementFeatures = "2AA5"
        public static let bootKeyboardInputReport = "2A22"
        public static let bootKeyboardOutputReport = "2A32"
        public static let bootMouseInputReport = "2A33"
        public static let bSSControlPoint = "2B2B"
        public static let bSSResponse = "2B2C"
        public static let cGMFeature = "2AA8"
        public static let cGMMeasurement = "2AA7"
        public static let cGMSessionRunTime = "2AAB"
        public static let cGMSessionStartTime = "2AAA"
        public static let cGMSpecificOpsControlPoint = "2AAC"
        public static let cGMStatus = "2AA9"
        public static let clientSupportedFeatures = "2B29"
        public static let crossTrainerData = "2ACE"
        public static let cSCFeature = "2A5C"
        public static let cSCMeasurement = "2A5B"
        public static let currentTime = "2A2B"
        public static let cyclingPowerControlPoint = "2A66"
        public static let cyclingPowerFeature = "2A65"
        public static let cyclingPowerMeasurement = "2A63"
        public static let cyclingPowerVector = "2A64"
        public static let databaseChangeIncrement = "2A99"
        public static let databaseHash = "2B2A"
        public static let dateofBirth = "2A85"
        public static let dateofThresholdAssessment = "2A86"
        public static let dateTime = "2A08"
        public static let dateUTC = "2AED"
        public static let dayDateTime = "2A0A"
        public static let dayofWeek = "2A09"
        public static let descriptorValueChanged = "2A7D"
        public static let dewPoint = "2A7B"
        public static let digital = "2A56"
        public static let digitalOutput = "2A57"
        public static let dSTOffset = "2A0D"
        public static let elevation = "2A6C"
        public static let emailAddress = "2A87"
        public static let emergencyID = "2B2D"
        public static let emergencyText = "2B2E"
        public static let exactTime100 = "2A0B"
        public static let exactTime256 = "2A0C"
        public static let fatBurnHeartRateLowerLimit = "2A88"
        public static let fatBurnHeartRateUpperLimit = "2A89"
        public static let firmware = "2A26"
        public static let firstName = "2A8A"
        public static let fitnessMachineControlPoint = "2AD9"
        public static let fitnessMachineFeature = "2ACC"
        public static let fitnessMachineStatus = "2ADA"
        public static let fiveZoneHeartRateLimits = "2A8B"
        public static let floorNumber = "2AB2"
        public static let centralAddressResolution = "2AA6"
        public static let deviceName = "2A00"
        public static let peripheralPreferredConnectionParameters = "2A04"
        public static let peripheralPrivacyFlag = "2A02"
        public static let reconnectionAddress = "2A03"
        public static let serviceChanged = "2A05"
        public static let gender = "2A8C"
        public static let glucoseFeature = "2A51"
        public static let glucoseMeasurement = "2A18"
        public static let glucoseMeasurementContext = "2A34"
        public static let gustFactor = "2A74"
        public static let hardware = "2A27"
        public static let heartRateControlPoint = "2A39"
        public static let heartRateMax = "2A8D"
        public static let heartRateMeasurement = "2A37"
        public static let heatIndex = "2A7A"
        public static let height = "2A8E"
        public static let hIDControlPoint = "2A4C"
        public static let hIDInformation = "2A4A"
        public static let hipCircumference = "2A8F"
        public static let hTTPControlPoint = "2ABA"
        public static let hTTPEntityBody = "2AB9"
        public static let hTTPHeaders = "2AB7"
        public static let hTTPStatusCode = "2AB8"
        public static let hTTPSSecurity = "2ABB"
        public static let humidity = "2A6F"
        public static let iDDAnnunciationStatus = "2B22"
        public static let iDDCommandControlPoint = "2B25"
        public static let iDDCommandData = "2B26"
        public static let iDDFeatures = "2B23"
        public static let iDDHistoryData = "2B28"
        public static let iDDRecordAccessControlPoint = "2B27"
        public static let iDDStatus = "2B21"
        public static let iDDStatusChanged = "2B20"
        public static let iDDStatusReaderControlPoint = "2B24"
        public static let iEEE1107320601RegulatoryCertificationDataList = "2A2A"
        public static let indoorBikeData = "2AD2"
        public static let indoorPositioningConfiguration = "2AAD"
        public static let intermediateCuffPressure = "2A36"
        public static let intermediateTemperature = "2A1E"
        public static let irradiance = "2A77"
        public static let language = "2AA2"
        public static let lastName = "2A90"
        public static let latitude = "2AAE"
        public static let lNControlPoint = "2A6B"
        public static let lNFeature = "2A6A"
        public static let localEastCoordinate = "2AB1"
        public static let localNorthCoordinate = "2AB0"
        public static let localTimeInformation = "2A0F"
        public static let locationandSpeedCharacteristic = "2A67"
        public static let locationName = "2AB5"
        public static let longitude = "2AAF"
        public static let magneticDeclination = "2A2C"
        public static let magneticFluxDensity2D = "2AA0"
        public static let magneticFluxDensity3D = "2AA1"
        public static let manufacturer = "2A29"
        public static let maximumRecommendedHeartRate = "2A91"
        public static let measurementInterval = "2A21"
        public static let model = "2A24"
        public static let navigation = "2A68"
        public static let networkAvailability = "2A3E"
        public static let newAlert = "2A46"
        public static let objectActionControlPoint = "2AC5"
        public static let objectChanged = "2AC8"
        public static let objectFirstCreated = "2AC1"
        public static let objectID = "2AC3"
        public static let objectLastModified = "2AC2"
        public static let objectListControlPoint = "2AC6"
        public static let objectListFilter = "2AC7"
        public static let objectName = "2ABE"
        public static let objectProperties = "2AC4"
        public static let objectSize = "2AC0"
        public static let objectType = "2ABF"
        public static let oTSFeature = "2ABD"
        public static let pLXContinuousMeasurementCharacteristic = "2A5F"
        public static let pLXFeatures = "2A60"
        public static let pLXSpotCheckMeasurement = "2A5E"
        public static let pnPID = "2A50"
        public static let pollenConcentration = "2A75"
        public static let position2D = "2A2F"
        public static let position3D = "2A30"
        public static let positionQuality = "2A69"
        public static let pressure = "2A6D"
        public static let protocolMode = "2A4E"
        public static let pulseOximetryControlPoint = "2A62"
        public static let rainfall = "2A78"
        public static let rCFeature = "2B1D"
        public static let rCSettings = "2B1E"
        public static let reconnectionConfigurationControlPoint = "2B1F"
        public static let recordAccessControlPoint = "2A52"
        public static let referenceTimeInformation = "2A14"
        public static let registeredUserCharacteristic = "2B37"
        public static let removable = "2A3A"
        public static let report = "2A4D"
        public static let reportMap = "2A4B"
        public static let resolvablePrivateAddressOnly = "2AC9"
        public static let restingHeartRate = "2A92"
        public static let ringerControlpoint = "2A40"
        public static let ringerSetting = "2A41"
        public static let rowerData = "2AD1"
        public static let rSCFeature = "2A54"
        public static let rSCMeasurement = "2A53"
        public static let sCControlPoint = "2A55"
        public static let scanIntervalWindow = "2A4F"
        public static let scanRefresh = "2A31"
        public static let scientificTemperatureCelsius = "2A3C"
        public static let secondaryTimeZone = "2A10"
        public static let sensorLocation = "2A5D"
        public static let serial = "2A25"
        public static let serverSupportedFeatures = "2B3A"
        public static let serviceRequired = "2A3B"
        public static let software = "2A28"
        public static let sportTypeforAerobicandAnaerobicThresholds = "2A93"
        public static let stairClimberData = "2AD0"
        public static let stepClimberData = "2ACF"
        public static let string = "2A3D"
        public static let supportedHeartRateRange = "2AD7"
        public static let supportedInclinationRange = "2AD5"
        public static let supportedNewAlertCategory = "2A47"
        public static let supportedPowerRange = "2AD8"
        public static let supportedResistanceLevelRange = "2AD6"
        public static let supportedSpeedRange = "2AD4"
        public static let supportedUnreadAlertCategory = "2A48"
        public static let systemID = "2A23"
        public static let tDSControlPoint = "2ABC"
        public static let temperature = "2A6E"
        public static let temperatureCelsius = "2A1F"
        public static let temperatureFahrenheit = "2A20"
        public static let temperatureMeasurement = "2A1C"
        public static let temperatureType = "2A1D"
        public static let threeZoneHeartRateLimits = "2A94"
        public static let timeAccuracy = "2A12"
        public static let timeBroadcast = "2A15"
        public static let timeSource = "2A13"
        public static let timeUpdateControlPoint = "2A16"
        public static let timeUpdateState = "2A17"
        public static let timewithDST = "2A11"
        public static let timeZone = "2A0E"
        public static let trainingStatus = "2AD3"
        public static let treadmillData = "2ACD"
        public static let trueWindDirection = "2A71"
        public static let trueWindSpeed = "2A70"
        public static let twoZoneHeartRateLimit = "2A95"
        public static let txPowerLevel = "2A07"
        public static let uncertainty = "2AB4"
        public static let unreadAlertStatus = "2A45"
        public static let uRI = "2AB6"
        public static let userControlPoint = "2A9F"
        public static let userIndex = "2A9A"
        public static let uVIndex = "2A76"
        public static let vO2Max = "2A96"
        public static let waistCircumference = "2A97"
        public static let weight = "2A98"
        public static let weightMeasurement = "2A9D"
        public static let weightScaleFeature = "2A9E"
        public static let windChill = "2A79"

        public static let devieInfoServiceUUIDs: [CBUUID] = {
            var output: [CBUUID] = []
            output.append(CBUUID(string: BLECharacteristic.manufacturer))
            output.append(CBUUID(string: BLECharacteristic.model))
            output.append(CBUUID(string: BLECharacteristic.serial))
            output.append(CBUUID(string: BLECharacteristic.hardware))
            output.append(CBUUID(string: BLECharacteristic.firmware))
            return output
        }()

        public static let battery = "2A19"
        public static let batteryServiceUUIDs: [CBUUID] = {
            var output: [CBUUID] = []
            output.append(CBUUID(string: BLECharacteristic.battery))
            return output
        }()
        
        public static let all: [String] = [manufacturer, model, serial, hardware, firmware, battery]
        public static let nameDictionary: [String: String] = [
            aerobicHeartRateLowerLimit: "AerobicHeartRateLowerLimit",
            aerobicHeartRateUpperLimit: "AerobicHeartRateUpperLimit",
            aerobicThreshold: "AerobicThreshold",
            age: "Age",
            aggregate: "Aggregate",
            alertCategoryID: "AlertCategoryID",
            alertCategoryIDBitMask: "AlertCategoryIDBitMask",
            alertLevel: "AlertLevel",
            alertNotificationControlPoint: "AlertNotificationControlPoint",
            alertStatus: "AlertStatus",
            altitude: "Altitude",
            anaerobicHeartRateLowerLimit: "AnaerobicHeartRateLowerLimit",
            anaerobicHeartRateUpperLimit: "AnaerobicHeartRateUpperLimit",
            anaerobicThreshold: "AnaerobicThreshold",
            analog: "Analog",
            analogOutput: "AnalogOutput",
            apparentWindDirection: "ApparentWindDirection",
            apparentWindSpeed: "ApparentWindSpeed",
            appearance: "Appearance",
            barometricPressureTrend: "BarometricPressureTrend",
            batteryLevel: "BatteryLevel",
            batteryLevelState: "BatteryLevelState",
            batteryPowerState: "BatteryPowerState",
            bloodPressureFeature: "BloodPressureFeature",
            bloodPressureMeasurement: "BloodPressureMeasurement",
            bodyCompositionFeature: "BodyCompositionFeature",
            bodyCompositionMeasurement: "BodyCompositionMeasurement",
            bodySensorLocation: "BodySensorLocation",
            bondManagementControlPoint: "BondManagementControlPoint",
            bondManagementFeatures: "BondManagementFeatures",
            bootKeyboardInputReport: "BootKeyboardInputReport",
            bootKeyboardOutputReport: "BootKeyboardOutputReport",
            bootMouseInputReport: "BootMouseInputReport",
            bSSControlPoint: "BSSControlPoint",
            bSSResponse: "BSSResponse",
            cGMFeature: "CGMFeature",
            cGMMeasurement: "CGMMeasurement",
            cGMSessionRunTime: "CGMSessionRunTime",
            cGMSessionStartTime: "CGMSessionStartTime",
            cGMSpecificOpsControlPoint: "CGMSpecificOpsControlPoint",
            cGMStatus: "CGMStatus",
            clientSupportedFeatures: "ClientSupportedFeatures",
            crossTrainerData: "CrossTrainerData",
            cSCFeature: "CSCFeature",
            cSCMeasurement: "CSCMeasurement",
            currentTime: "CurrentTime",
            cyclingPowerControlPoint: "CyclingPowerControlPoint",
            cyclingPowerFeature: "CyclingPowerFeature",
            cyclingPowerMeasurement: "CyclingPowerMeasurement",
            cyclingPowerVector: "CyclingPowerVector",
            databaseChangeIncrement: "DatabaseChangeIncrement",
            databaseHash: "DatabaseHash",
            dateofBirth: "DateofBirth",
            dateofThresholdAssessment: "DateofThresholdAssessment",
            dateTime: "DateTime",
            dateUTC: "DateUTC",
            dayDateTime: "DayDateTime",
            dayofWeek: "DayofWeek",
            descriptorValueChanged: "DescriptorValueChanged",
            dewPoint: "DewPoint",
            digital: "Digital",
            digitalOutput: "DigitalOutput",
            dSTOffset: "DSTOffset",
            elevation: "Elevation",
            emailAddress: "EmailAddress",
            emergencyID: "EmergencyID",
            emergencyText: "EmergencyText",
            exactTime100: "ExactTime100",
            exactTime256: "ExactTime256",
            fatBurnHeartRateLowerLimit: "FatBurnHeartRateLowerLimit",
            fatBurnHeartRateUpperLimit: "FatBurnHeartRateUpperLimit",
            firmware: "Firmware",
            firstName: "FirstName",
            fitnessMachineControlPoint: "FitnessMachineControlPoint",
            fitnessMachineFeature: "FitnessMachineFeature",
            fitnessMachineStatus: "FitnessMachineStatus",
            fiveZoneHeartRateLimits: "FiveZoneHeartRateLimits",
            floorNumber: "FloorNumber",
            centralAddressResolution: "CentralAddressResolution",
            deviceName: "DeviceName",
            peripheralPreferredConnectionParameters: "PeripheralPreferredConnectionParameters",
            peripheralPrivacyFlag: "PeripheralPrivacyFlag",
            reconnectionAddress: "ReconnectionAddress",
            serviceChanged: "ServiceChanged",
            gender: "Gender",
            glucoseFeature: "GlucoseFeature",
            glucoseMeasurement: "GlucoseMeasurement",
            glucoseMeasurementContext: "GlucoseMeasurementContext",
            gustFactor: "GustFactor",
            hardware: "Hardware",
            heartRateControlPoint: "HeartRateControlPoint",
            heartRateMax: "HeartRateMax",
            heartRateMeasurement: "HeartRateMeasurement",
            heatIndex: "HeatIndex",
            height: "Height",
            hIDControlPoint: "HIDControlPoint",
            hIDInformation: "HIDInformation",
            hipCircumference: "HipCircumference",
            hTTPControlPoint: "HTTPControlPoint",
            hTTPEntityBody: "HTTPEntityBody",
            hTTPHeaders: "HTTPHeaders",
            hTTPStatusCode: "HTTPStatusCode",
            hTTPSSecurity: "HTTPSSecurity",
            humidity: "Humidity",
            iDDAnnunciationStatus: "IDDAnnunciationStatus",
            iDDCommandControlPoint: "IDDCommandControlPoint",
            iDDCommandData: "IDDCommandData",
            iDDFeatures: "IDDFeatures",
            iDDHistoryData: "IDDHistoryData",
            iDDRecordAccessControlPoint: "IDDRecordAccessControlPoint",
            iDDStatus: "IDDStatus",
            iDDStatusChanged: "IDDStatusChanged",
            iDDStatusReaderControlPoint: "IDDStatusReaderControlPoint",
            iEEE1107320601RegulatoryCertificationDataList: "IEEE1107320601RegulatoryCertificationDataList",
            indoorBikeData: "IndoorBikeData",
            indoorPositioningConfiguration: "IndoorPositioningConfiguration",
            intermediateCuffPressure: "IntermediateCuffPressure",
            intermediateTemperature: "IntermediateTemperature",
            irradiance: "Irradiance",
            language: "Language",
            lastName: "LastName",
            latitude: "Latitude",
            lNControlPoint: "LNControlPoint",
            lNFeature: "LNFeature",
            localEastCoordinate: "LocalEastCoordinate",
            localNorthCoordinate: "LocalNorthCoordinate",
            localTimeInformation: "LocalTimeInformation",
            locationandSpeedCharacteristic: "LocationandSpeedCharacteristic",
            locationName: "LocationName",
            longitude: "Longitude",
            magneticDeclination: "MagneticDeclination",
            magneticFluxDensity2D: "MagneticFluxDensity2D",
            magneticFluxDensity3D: "MagneticFluxDensity3D",
            manufacturer: "Manufacturer",
            maximumRecommendedHeartRate: "MaximumRecommendedHeartRate",
            measurementInterval: "MeasurementInterval",
            model: "Model",
            navigation: "Navigation",
            networkAvailability: "NetworkAvailability",
            newAlert: "NewAlert",
            objectActionControlPoint: "ObjectActionControlPoint",
            objectChanged: "ObjectChanged",
            objectFirstCreated: "ObjectFirstCreated",
            objectID: "ObjectID",
            objectLastModified: "ObjectLastModified",
            objectListControlPoint: "ObjectListControlPoint",
            objectListFilter: "ObjectListFilter",
            objectName: "ObjectName",
            objectProperties: "ObjectProperties",
            objectSize: "ObjectSize",
            objectType: "ObjectType",
            oTSFeature: "OTSFeature",
            pLXContinuousMeasurementCharacteristic: "PLXContinuousMeasurementCharacteristic",
            pLXFeatures: "PLXFeatures",
            pLXSpotCheckMeasurement: "PLXSpotCheckMeasurement",
            pnPID: "PnPID",
            pollenConcentration: "PollenConcentration",
            position2D: "Position2D",
            position3D: "Position3D",
            positionQuality: "PositionQuality",
            pressure: "Pressure",
            protocolMode: "ProtocolMode",
            pulseOximetryControlPoint: "PulseOximetryControlPoint",
            rainfall: "Rainfall",
            rCFeature: "RCFeature",
            rCSettings: "RCSettings",
            reconnectionConfigurationControlPoint: "ReconnectionConfigurationControlPoint",
            recordAccessControlPoint: "RecordAccessControlPoint",
            referenceTimeInformation: "ReferenceTimeInformation",
            registeredUserCharacteristic: "RegisteredUserCharacteristic",
            removable: "Removable",
            report: "Report",
            reportMap: "ReportMap",
            resolvablePrivateAddressOnly: "ResolvablePrivateAddressOnly",
            restingHeartRate: "RestingHeartRate",
            ringerControlpoint: "RingerControlpoint",
            ringerSetting: "RingerSetting",
            rowerData: "RowerData",
            rSCFeature: "RSCFeature",
            rSCMeasurement: "RSCMeasurement",
            sCControlPoint: "SCControlPoint",
            scanIntervalWindow: "ScanIntervalWindow",
            scanRefresh: "ScanRefresh",
            scientificTemperatureCelsius: "ScientificTemperatureCelsius",
            secondaryTimeZone: "SecondaryTimeZone",
            sensorLocation: "SensorLocation",
            serial: "Serial",
            serverSupportedFeatures: "ServerSupportedFeatures",
            serviceRequired: "ServiceRequired",
            software: "Software",
            sportTypeforAerobicandAnaerobicThresholds: "SportTypeforAerobicandAnaerobicThresholds",
            stairClimberData: "StairClimberData",
            stepClimberData: "StepClimberData",
            string: "String",
            supportedHeartRateRange: "SupportedHeartRateRange",
            supportedInclinationRange: "SupportedInclinationRange",
            supportedNewAlertCategory: "SupportedNewAlertCategory",
            supportedPowerRange: "SupportedPowerRange",
            supportedResistanceLevelRange: "SupportedResistanceLevelRange",
            supportedSpeedRange: "SupportedSpeedRange",
            supportedUnreadAlertCategory: "SupportedUnreadAlertCategory",
            systemID: "SystemID",
            tDSControlPoint: "TDSControlPoint",
            temperature: "Temperature",
            temperatureCelsius: "TemperatureCelsius",
            temperatureFahrenheit: "TemperatureFahrenheit",
            temperatureMeasurement: "TemperatureMeasurement",
            temperatureType: "TemperatureType",
            threeZoneHeartRateLimits: "ThreeZoneHeartRateLimits",
            timeAccuracy: "TimeAccuracy",
            timeBroadcast: "TimeBroadcast",
            timeSource: "TimeSource",
            timeUpdateControlPoint: "TimeUpdateControlPoint",
            timeUpdateState: "TimeUpdateState",
            timewithDST: "TimewithDST",
            timeZone: "TimeZone",
            trainingStatus: "TrainingStatus",
            treadmillData: "TreadmillData",
            trueWindDirection: "TrueWindDirection",
            trueWindSpeed: "TrueWindSpeed",
            twoZoneHeartRateLimit: "TwoZoneHeartRateLimit",
            txPowerLevel: "TxPowerLevel",
            uncertainty: "Uncertainty",
            unreadAlertStatus: "UnreadAlertStatus",
            uRI: "URI",
            userControlPoint: "UserControlPoint",
            userIndex: "UserIndex",
            uVIndex: "UVIndex",
            vO2Max: "VO2Max",
            waistCircumference: "WaistCircumference",
            weight: "Weight",
            weightMeasurement: "WeightMeasurement",
            weightScaleFeature: "WeightScaleFeature",
            windChill: "WindChill"
        ]
    }

    struct IOManager {
        private var expectedServices = Set<String>(BLEService.all)
        private var expectedCharacteristics = Set<String>(BLECharacteristic.all)

        private(set) var foundServices: [String: CBService] = [:]
        private(set) var foundCharacteristics: [String: CBCharacteristic] = [:]

        private(set) var expectedToRead: [String: String?] = [:]
        //private var hasRead: [String: CBCharacteristic] = [:]

        mutating func discovered(service: CBService) {
            expectedServices.remove(service.uuid.uuidString)
            foundServices[service.uuid.uuidString] = service
        }

        mutating func discovered(characteristic: CBCharacteristic) {
            expectedCharacteristics.remove(characteristic.uuid.uuidString)
            foundCharacteristics[characteristic.uuid.uuidString] = characteristic
            expectedToRead[characteristic.uuid.uuidString] = nil
        }

        mutating func didRead(characteristic: CBCharacteristic, string: String) {
            expectedToRead[characteristic.uuid.uuidString] = string
        }

        var hasDiscoverdAllServicesAndCharacteristics: Bool {
            expectedServices.isEmpty && expectedCharacteristics.isEmpty
        }
        
        var hasReadAllCharacteristics: Bool {
            (expectedToRead.values.filter { $0 == nil }).count == 0
        }
    }
}

extension Collection where Element: BLEDevice {
    func device(for peripheral: CBPeripheral) -> BLEDevice? {
        guard let device = (self.first { $0.peripheral == peripheral }) else {
            return nil
        }
        return device
    }
}

//extension BLEDevice {
//    struct AdvertisementDataCache {
//        private(set) var advertisementData: [[String: Any]] = []
//        private(set) var rssis: [Float] = []
//
//        mutating func append(advertisementData: [String: Any],
//                             rssi: Float) {
//                //var ad = (map[peripheral.identifier]) ?? [String: Any]()
//                //for (key, value) in advertisementData {
//                //    ad[key] = value
//                //}
//                //map[peripheral.identifier] = ad
//                //
//                //return ad
//        }
//
//        mutating func reset() {
//            advertisementData.removeAll()
//            rssis.removeAll()
//        }
//    }
//}

extension BLEDevice {
    public var connectButtonTitle: String {
        switch peripheral.state {
        case .connected: return "Disconnect"
        default: return "Connect"
        }
    }
}
