//
//  CoreDataStack.swift
//  CodeGen
//
//  Created by Zakk Hoyt on 12/8/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import Foundation
import CoreData

// This extension contains static vars which are useful for injection
//
//    // Example:
//    let databaseUrl = CoreDataStack.groupUrl.appendingPathComponent(CoreDataStack.name).appendingPathExtension("sqlite")
//    let managedObjectModel = CoreDataStack.managedObjectModel
//    coreDataStack = CoreDataStack(managedObjectModel: managedObjectModel, databaseUrl: databaseUrl)

extension CoreDataStack {
    static let name = "BLESnooping"
    static let group = "group.com.nightlight.BLESnoopingKit"
    
    static public var managedObjectModel: NSManagedObjectModel = {
        let bundle = Bundle.init(for: CoreDataStack.self)
        let modelURL = bundle.url(forResource: name, withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    static public var groupUrl: URL {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: group)!
    }
    
    static public var documentsUrl: URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        return URL(fileURLWithPath: documentsPath!)
    }
}

public class CoreDataStack {
    static let shared = CoreDataStack()
    
    var documentsDirURL: URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        return URL(fileURLWithPath: documentsPath!)
    }
    
    var bundle: Bundle {
        Bundle.init(for: CoreDataStack.self)
    }
    
    var modelURL: URL {
        bundle.url(forResource: "BLESnooping", withExtension: "momd")!
    }

    lazy var managedObjectModel: NSManagedObjectModel = {
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = documentsDirURL.appendingPathComponent("Nightlight.sqlite")
        
        do {
            let options = [
                NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true
            ]

            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
            Logger.debug("CoreData", "Loaded persistentStoreCoordinator from:\n\(url.path)")
        } catch let  error as NSError {
            Logger.critical("CoreData", "Error adding persistent store: " + error.localizedDescription)
        }
        return coordinator
    }()

    lazy var mainContext: NSManagedObjectContext = {
        var context = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        context.persistentStoreCoordinator = self.persistentStoreCoordinator
        return context
    }()

    lazy var backgroundContext: NSManagedObjectContext = {
        var context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        context.parent = self.mainContext
        return context
    }()
}
