//
//  CDPeripheral+Helpers.swift
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/8/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import Foundation
import CoreData

extension CDPeripheral {
    public static func create(context: NSManagedObjectContext,
                              configure: ((CDPeripheral) -> Void)?,
                              completion: @escaping (Result<CDPeripheral, Error>) -> Void) {
        context.perform {
            let entity = CDPeripheral(entity: CDPeripheral.entity(), insertInto: context)
            configure?(entity)
            do {
                try context.save()
                Logger.debug("CoreData", "Created and saved entity: \(entity.entity)")
                completion(Result { entity })
            } catch {
                Logger.error(error)
                completion(Result { throw error })
            }
        }
    }
    
    public static func createOrUpdate(context: NSManagedObjectContext,
                                      predicateProvider: @escaping () -> NSPredicate,
                                      completion: @escaping (Result<CDPeripheral, Error>) -> Void) {
        context.perform {
            let fetchRequest: NSFetchRequest<CDPeripheral> = CDPeripheral.fetchRequest()
            let predicate = predicateProvider()
            fetchRequest.predicate = predicate
            do {
                let entities = try context.fetch(fetchRequest)
                if let first = entities.first {
                    Logger.debug("CoreData", "Fetched entity from predicate: \(predicate)")
                    completion(Result { first })
                    return
                }
                
                CDPeripheral.create(context: context, configure: nil, completion: completion)
                // We didnt' find one, so let's create one
            } catch {
                completion(Result { throw error })
            }
        }
    }
    
    public func deleteAndSave(context: NSManagedObjectContext,
                              completion: @escaping (Result<NSNull, Error>) -> Void) {
        context.perform {
            do {
                let entityDescription = self.description
                context.delete(self)
                try context.save()
                Logger.debug("CoreData", "Deleted entity and saved: \(entityDescription)")
                completion(Result { NSNull() })
            } catch {
                completion(Result { throw error })
            }
        }
    }
}

extension Collection where Element: NSManagedObject {
    public func deleteAndSave(context: NSManagedObjectContext,
                              completion: @escaping (Result<NSNull, Error>) -> Void) {
        context.perform {
            do {
                let entityDescriptions = self
                    .map { $0.description }
                    .joined(separator: "\n")
                self.forEach { context.delete($0) }
                try context.save()
                Logger.debug("CoreData", "Deleted entities: \(entityDescriptions)")
                completion(Result { NSNull() })
            } catch {
                completion(Result { throw error })
            }
        }
    }
}

extension NSManagedObjectContext {
    func deleteAndSave(entities: [NSManagedObject],
                       completion: @escaping (Result<NSNull, Error>) -> Void) {
        self.perform {
            do {
                entities.forEach { self.delete($0) }
                try self.save()
                completion(Result { NSNull() })
            } catch {
                completion(Result { throw error })
            }
        }
    }
}
