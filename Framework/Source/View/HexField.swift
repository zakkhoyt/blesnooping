//
//  HexField.swift
//  HexTextField
//
//  Created by Zakk Hoyt on 12/7/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

protocol KeyboardKey: Hashable, RawRepresentable where Self.RawValue == Int {
    var text: String { get }
}

extension KeyboardKey {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.text == rhs.text
    }
}

enum ControlKey: Int, KeyboardKey, CaseIterable {
    case backspace
    case done

    var text: String {
        switch self {
        case .done: return "Done"
        case .backspace: return "Delete"
        }
    }
}

enum HexKey: Int, KeyboardKey, CaseIterable {
    case a = 0xA
    case b = 0xB
    case c = 0xC
    case d = 0xD
    
    case seven = 0x7
    case eight = 0x8
    case nine = 0x9
    case e = 0xE

    case four = 0x4
    case five = 0x5
    case six = 0x6
    case f = 0xF

    case one = 0x1
    case two = 0x2
    case three = 0x3
    case zero = 0x0
    
    var text: String {
        return String(format: "%X", rawValue)
    }
}

class KeyboardView<T: KeyboardKey & CaseIterable>: UIView {
    var keyPressed: ((T) -> Void)?
    var buttons: [T: UIButton] = [:]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        backgroundColor = .clear
        for key in T.allCases {
            let button = UIButton(frame: .zero)
            button.tag = key.rawValue
            button.backgroundColor = .clear
            button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .title3)
            button.setTitle(key.text, for: .normal)
            button.setTitleColor(.label, for: .normal)
            button.setTitleColor(.secondaryLabel, for: .highlighted)
            button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            addSubview(button)
            buttons[key] = button
        }
    }
    
    @objc private func buttonAction(sender: UIButton) {
        guard let hexKey = T(rawValue: sender.tag) else {
            assert(false, "hexKey not found from button")
            return
        }
        keyPressed?(hexKey)
    }
}

class ControlKeyboardView: KeyboardView<ControlKey> {
    override func layoutSubviews() {
        super.layoutSubviews()
        let gutter: CGFloat = 4
        
        let widthCount = 2
        let baseWidth = bounds.width - (CGFloat(widthCount + 1) * gutter)
        let width = baseWidth / CGFloat(widthCount)
        
        let heightCount = 1
        let baseHeight = bounds.height - (CGFloat(heightCount + 1) * gutter)
        let height = baseHeight / CGFloat(heightCount)
        
        for (i, controlKey) in ControlKey.allCases.enumerated() {
            guard let button = buttons[controlKey] else {
                assert(false, "no button from key")
                continue
            }
            let x = i % widthCount
            let y = i / widthCount

            let frame = CGRect(x: (CGFloat(x + 1) * gutter) + (CGFloat(x) * width),
                               y: (CGFloat(y + 1) * gutter) + (CGFloat(y) * height),
                               width: width,
                               height: height)
            button.frame = frame
        }
    }
}

class HexKeyboardView: KeyboardView<HexKey> {
    override func layoutSubviews() {
        super.layoutSubviews()
        let gutter: CGFloat = 4
        
        let widthCount = 4
        let baseWidth = bounds.width - (CGFloat(widthCount + 1) * gutter)
        let width = baseWidth / CGFloat(widthCount)
        
        let heightCount = 4
        let baseHeight = bounds.height - (CGFloat(heightCount + 1) * gutter)
        let height = baseHeight / CGFloat(heightCount)
        
        for (i, hexKey) in HexKey.allCases.enumerated() {
            guard let button = buttons[hexKey] else {
                assert(false, "no button from key")
                continue
            }
            let x = i % widthCount
            let y = i / widthCount
            
            let frame = CGRect(x: (CGFloat(x + 1) * gutter) + (CGFloat(x) * width),
                               y: (CGFloat(y + 1) * gutter) + (CGFloat(y) * height),
                               width: width,
                               height: height)
            button.frame = frame
        }
    }
}

class CombinedKeyboardView: UIView {
    var hexKeyPressed: ((HexKey) -> Void)?
    var controlKeyPressed: ((ControlKey) -> Void)?
    
    private var controlKeyboardView = ControlKeyboardView(frame: .zero)
    private var hexKeyboardView = HexKeyboardView(frame: .zero)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        #if os(tvOS)
        backgroundColor = .systemGray
        #else
        backgroundColor = .systemGray3
        #endif
        
        // Configure layout
        controlKeyboardView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(controlKeyboardView)
        controlKeyboardView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        controlKeyboardView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        controlKeyboardView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        controlKeyboardView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        
        hexKeyboardView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(hexKeyboardView)
        hexKeyboardView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        hexKeyboardView.topAnchor.constraint(equalTo: controlKeyboardView.bottomAnchor).isActive = true
        hexKeyboardView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true

        hexKeyboardView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor).isActive = true
        hexKeyboardView.heightAnchor.constraint(equalToConstant: 44 * 4).isActive = true

        // Configure callbacks
        hexKeyboardView.keyPressed = { [weak self] hexKey in
            self?.hexKeyPressed?(hexKey)
        }
        
        controlKeyboardView.keyPressed = { [weak self] controlKey in
            self?.controlKeyPressed?(controlKey)
        }
    }
}

//class LiveColorAccessoryView: UIView {
//    #if os(iOS)
//
//    var liveSwitchToggled: ((UISwitch) -> Void)?
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        commonInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        commonInit()
//    }
//
//    private func commonInit() {
//        let liveSwitch = UISwitch(frame: .zero)
//        liveSwitch.isOn = true
//        liveSwitch.addTarget(self, action: #selector(liveSwitchValueChanged), for: .valueChanged)
//        liveSwitch.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(liveSwitch)
//        liveSwitch.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
//        liveSwitch.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//
//        let liveLabel = UILabel(frame: .zero)
//        //liveLabel.textAlignment = .right
//        liveLabel.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(liveLabel)
//        liveLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
//        liveLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
//        liveLabel.trailingAnchor.constraint(equalTo: liveSwitch.leadingAnchor, constant: -8).isActive = true
//        liveLabel.textColor = .label
//        liveLabel.text = "Live Colors"
//    }
//
//    @objc
//    private func liveSwitchValueChanged(sender: UISwitch) {
//        liveSwitchToggled?(sender)
//    }
//    #endif
//}

public protocol HexFieldColorDelegate: AnyObject {
    func hexField(_ hexField: HexField, didUpdateColor color: UIColor?)
}

public protocol HexFieldHexStringDelegate: AnyObject {
    func hexField(_ hexField: HexField, didUpdateHexString text: String?)
}

open class HexField: UITextField {
    // TODO: When setting text, examine for hexString chars only
    
    public weak var colorDelegate: HexFieldColorDelegate?
    public weak var hexStringDelegate: HexFieldHexStringDelegate?
    
    public var liveBackgroundColor: Bool = true
    
    public private(set) var color: UIColor = .black
    
    // swiftlint:disable implicitly_unwrapped_optional
    private var keyboardView: CombinedKeyboardView!
    // swiftlint:enable implicitly_unwrapped_optional
    
    //private var liveColorAccessoryView = LiveColorAccessoryView()
    
    open override var text: String? {
        set {
            guard let text = newValue else {
                super.text = nil
                return
            }
            //super.text = String(text.prefix(6))
            super.text = text
        }
        get {
            guard let text = super.text else {
                return nil
            }
            //return String(text.prefix(6))
            return text
        }
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func updateDelegates() {
        if let text = self.text {
            let color = UIColor(hexString: text)
            self.colorDelegate?.hexField(self, didUpdateColor: color)
            self.hexStringDelegate?.hexField(self, didUpdateHexString: text)
        } else {
            self.colorDelegate?.hexField(self, didUpdateColor: nil)
            self.hexStringDelegate?.hexField(self, didUpdateHexString: nil)
        }
    }
    
    private func updateBackgroundColor() {
//        guard liveBackgroundColor,
//            let text = self.text else {
//                keyboardView.backgroundColor = .lightGray
//                liveColorAccessoryView.backgroundColor = .lightGray
//            return
//        }
//        keyboardView.backgroundColor = UIColor(hexString: text)
//        liveColorAccessoryView.backgroundColor = UIColor(hexString: text)
    }
    
    private func commonInit() {
        let width = UIScreen.main.bounds.width
        let height = min(284, width / 1.4)
        let frame = CGRect(x: 0, y: 0, width: width, height: height)
        
        keyboardView = CombinedKeyboardView(frame: frame)
        
        keyboardView.controlKeyPressed = { [weak self] controlKey in
            guard let safeSelf = self else { return }
            var output = safeSelf.text ?? ""
            switch controlKey {
            case .done:
                safeSelf.updateDelegates()
                safeSelf.updateBackgroundColor()
                safeSelf.sendActions(for: .editingDidEnd)
                safeSelf.resignFirstResponder()
            case .backspace:
                if output.count > 0 {
                    output.removeLast()
                    safeSelf.text = output
                    safeSelf.sendActions(for: .editingChanged)
                    safeSelf.updateDelegates()
                    safeSelf.updateBackgroundColor()
                }
            }
        }
        keyboardView.hexKeyPressed = { [weak self] hexKey in
            guard let safeSelf = self else { return }
            var output = safeSelf.text ?? ""
            output += hexKey.text
            safeSelf.text = output
            safeSelf.sendActions(for: .editingChanged)
            safeSelf.updateDelegates()
            safeSelf.updateBackgroundColor()
        }
//        liveColorAccessoryView.liveSwitchToggled = { [weak self] sender in
//            guard let safeSelf = self else { return }
//            safeSelf.liveBackgroundColor = sender.isOn
//            safeSelf.updateDelegates()
//            safeSelf.updateBackgroundColor()
//        }
//
        inputView = keyboardView
//        inputAccessoryView = liveColorAccessoryView
//        inputAccessoryView?.frame = CGRect(x: 0, y: 0, width: width, height: 44)
        
        text = ""
        
        updateDelegates()
        updateBackgroundColor()
    }
}
