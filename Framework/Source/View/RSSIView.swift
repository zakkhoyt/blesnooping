//
//  RSSIView.swift
//  GrowScale
//
//  Created by Zakk Hoyt on 3/27/17.
//  Copyright © 2017 Zakk Hoyt. All rights reserved.
//

import UIKit

public class RSSIView: UIView {
    public static let unknownRSSI: Float = -127

    public var isLabelHidden: Bool = true
    
    // MARK: Public variables
    @IBInspectable
    public var rssi: Float = -75 {
        didSet {
            update()
        }
    }

    private let stackView = UIStackView()
    private let imageView = UIImageView()
    private let label = UILabel()

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 44, height: UIView.noIntrinsicMetric)
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stackView)
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        label.font = UIFont.preferredFont(forTextStyle: .caption2)
        stackView.addArrangedSubview(label)
        
        imageView.contentMode = .scaleToFill
        stackView.addArrangedSubview(imageView)
        
        update()
    }

    // MARK: Private methods
    private func update() {
        imageView.image = {
            switch abs(rssi) {
            case 75..<90: return UIImage(named: "rssi1_dark")
            case 60..<75: return UIImage(named: "rssi2_dark")
            case 50..<60: return UIImage(named: "rssi3_dark")
            case 00..<50: return UIImage(named: "rssi4_dark")
            default: return UIImage(named: "rssi0_dark")
            }
        }()
        
        label.isHidden = isLabelHidden
        label.text = String(format: "%d", Int(rssi))
    }
}
