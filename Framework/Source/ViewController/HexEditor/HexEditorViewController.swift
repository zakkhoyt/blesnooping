//
//  HexEditorViewController.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/12/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit

internal class HexEditor {
    
}

public class HexEditorViewController: UIViewController {
    public enum WordSize: Int, CustomStringConvertible, CaseIterable {
        case none
        case eight
        case sixteen
        case thirtyTwo
        case sixtyFour

        public var description: String {
            switch self {
            case .none: return "None"
            case .eight: return "8"
            case .sixteen: return "16"
            case .thirtyTwo: return "32"
            case .sixtyFour: return "64"
            }
        }
        
        var characters: Int {
            switch self {
            case .none: return 0
            case .eight: return 2
            case .sixteen: return 4
            case .thirtyTwo: return 8
            case .sixtyFour: return 16
            }
        }
    }
    
    public var data: Data = Data() {
        didSet { refreshUI() }
    }
    
    // Represents a single [A-F,0-9] character as user is inputing data
    private var partialByte: String? {
        didSet { refreshUI() }
    }
    
    public var wordSize: WordSize = .none {
        didSet { refreshUI() }
    }
    
    private var previousSelectedRange: NSRange?
    
    // convert range from NSRange (in UITextView) to Range<Int> (in Data). Accounts for spaces.
    var selectedDataRange: Range<Int> {
        let selectedRange = dataTextView.selectedRange
        let numberOfSpacesStart: Int = {
            switch wordSize {
            case .none: return 0
            default: return selectedRange.location / (wordSize.characters + 1)
            }
        }()
        var start = selectedRange.location - numberOfSpacesStart
        if start % 2 == 1 { start -= 1 } // don't start range in the middle of a byte
        let numberOfSpacesEnd: Int = {
            switch wordSize {
            case .none: return 0
            default:
                let spacesBeforeLocation = (selectedRange.location + selectedRange.length) / (wordSize.characters + 1)
                return spacesBeforeLocation
            }
        }()
        var end = (selectedRange.location + selectedRange.length) - numberOfSpacesEnd
        if end % 2 == 1 { end -= 1 } // don't end range in the middle of a byte
        return (start/2)..<(end/2)
    }
    
    let stackView = UIStackView()
    let wordSizeSegment = UISegmentedControl()
    let addressTextView = UITextView()
    let dataTextView = UITextView()
    let selectedBinaryLabel = UILabel()
    let selectedHexLabel = UILabel()
    
    public init() {
        super.init(nibName: nil, bundle: nil)
        commonInit()
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func commonInit() {
        view.addSubview(stackView)
        
        stackView.axis = .vertical
        stackView.pinEdgesToParent()
        
        wordSizeSegment.removeAllSegments()
        WordSize.allCases.forEach { wordSizeSegment.insertSegment(withTitle: $0.description, at: $0.rawValue, animated: false) }
        wordSizeSegment.selectedSegmentIndex = wordSize.rawValue
        wordSizeSegment.addTarget(self, action: #selector(wordSizeSegmentValueChanged), for: .valueChanged)
        stackView.addArrangedSubview(wordSizeSegment)
        
        let fontSize: CGFloat = 20
        do {
            let dataView = UIView()
            
            addressTextView.backgroundColor = UIColor.systemGray2
            addressTextView.font = UIFont(name: "Courier", size: fontSize)
            addressTextView.delegate = self
            dataView.addSubview(addressTextView)
            addressTextView.pinEdgesToParent(useSafeAreaLayoutGuide: true)
            
            dataTextView.backgroundColor = UIColor.systemGray
            dataTextView.font = UIFont(name: "Courier", size: fontSize)
            dataTextView.delegate = self
            dataView.addSubview(dataTextView)
            dataTextView.pinEdgesToParent(useSafeAreaLayoutGuide: true, insets: UIEdgeInsets(top: 0, left: 44, bottom: 0, right: 0))
            
            stackView.addArrangedSubview(dataView)
        }
        
        selectedBinaryLabel.text = "Binary"
        selectedBinaryLabel.font = UIFont(name: "Courier", size: fontSize)
        stackView.addArrangedSubview(selectedBinaryLabel)
        
        selectedHexLabel.text = "Hex"
        selectedHexLabel.font = UIFont(name: "Courier", size: fontSize)
        stackView.addArrangedSubview(selectedHexLabel)
    }
    
    private func refreshUI() {
        // Pad out partial byte for display
        let lastByte: String = {
            guard let partialByte = partialByte else { return "" }
            return "0\(partialByte)".uppercased()
        }()
        
        // Insert spaces into data.hexString and display it. Appending lastByte
        let splited = data.hexString.split(length: wordSize.characters)
        let joined = splited.joined(separator: " ")
        dataTextView.text = joined + lastByte
        
        // Update selected labels
        func updateSelected() {
            guard !selectedDataRange.isEmpty else {
                selectedHexLabel.text = ""
                selectedBinaryLabel.text = ""
                return
            }

            let selectedData = data[selectedDataRange]
            selectedHexLabel.text = "0x" + (selectedData.hexString)
            selectedBinaryLabel.text = "0b" + (selectedData.binaryString)
        }
        updateSelected()
    }
    
    // MARK: IBActions
    
    @objc
    private func wordSizeSegmentValueChanged(sender: UISegmentedControl) {
        guard let wordSize = WordSize(rawValue: sender.selectedSegmentIndex) else { return }
        self.wordSize = wordSize
    }
}

extension HexEditorViewController: UITextViewDelegate {
    public func textView(_ textView: UITextView,
                         shouldChangeTextIn range: NSRange,
                         replacementText text: String) -> Bool {
        if text == "" {
            if partialByte != nil {
                partialByte = nil
                let bytesToDelete = (range.length - 1) / 2
                data.removeLast(bytesToDelete)
            } else {
                let lastByte = String(data.hexString.suffix(2))
                partialByte = String(lastByte.prefix(1))
                data.removeLast()
            }
            return false
        }
        guard text.isHexString else { return false }
        guard let partialByte = partialByte else {
            self.partialByte = text
            return false
        }
        let newByte = "\(partialByte)\(text)".uppercased()
        self.partialByte = nil
        guard let byte = newByte.hexadecimal else { return false }
        data.append(byte)
        return false
    }
    
    public func textViewDidChange(_ textView: UITextView) {
    }
    
    public func textViewDidChangeSelection(_ textView: UITextView) {
        guard let previousSelectedRange = previousSelectedRange else {
            self.previousSelectedRange = textView.selectedRange
            return
        }
        
        var locationOffset = 0
        var lengthOffset = 0
        let selectedRange = dataTextView.selectedRange
        let numberOfSpacesStart: Int = {
            switch wordSize {
            case .none: return 0
            default: return selectedRange.location / (wordSize.characters + 1)
            }
        }()
        let start = selectedRange.location - numberOfSpacesStart
        if start % 2 == 1 {
            if selectedRange.length > 0 {
                locationOffset = -1
            } else {
                if selectedRange.location >= previousSelectedRange.location {
                    locationOffset = +1
                } else {
                    locationOffset = -1
                }
            }
        } // don't start range in the middle of a byte
        let numberOfSpacesEnd: Int = {
            switch wordSize {
            case .none: return 0
            default:
                let spacesBeforeLocation = (selectedRange.location + selectedRange.length) / (wordSize.characters + 1)
                return spacesBeforeLocation
            }
        }()
        let end = (selectedRange.location + selectedRange.length) - numberOfSpacesEnd
        if end % 2 == 1 {
            if selectedRange.length > 0 {
                lengthOffset = 1
            } else {
                lengthOffset = -1
            }
        } // don't end range in the middle of a byte
//        return (start/2)..<(end/2)
        if locationOffset != 0 || lengthOffset != 0 {
            let range = NSRange(location: selectedRange.location + locationOffset,
//                                length: selectedRange.length + lengthOffset)
                length: selectedRange.length)
            dataTextView.selectedRange = range
            self.previousSelectedRange = range
        } else {
            self.previousSelectedRange = selectedRange
        }
        refreshUI()
    }
}

extension String {
    func split(length: Int) -> [String] {
        guard length > 0 else { return [self] }
        let range = 0..<((count+length - 1) / length)
        let indices = range.map { length * $0..<min(length*($0 + 1), count) }
        return indices.map { self.reversed().reversed() [$0.startIndex..<$0.endIndex] }.map { String($0) }
    }
}
