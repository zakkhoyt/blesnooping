//
//  BLESnoopingKitTests.swift
//  BLESnoopingKitTests
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import XCTest
@testable import BLESnoopingKit_iOS

class BLESnoopingKitTests: XCTestCase {
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testHexEditorCharacterMaps() {
        // swiftlint:disable line_length
        let input = "000102030405060708090A0B0C0D0E0F101112131415161718191A1B1C1D1E1F202122232425262728292A2B2C2D2E2F303132333435363738393A3B3C3D3E3F404142434445464748494A4B4C4D4E4F505152535455565758595A5B5C5D5E5F606162636465666768696A6B6C6D6E6F707172737475767778797A7B7C7D7E7F808182838485868788898A8B8C8D8E8F909192939495969798999A9B9C9D9E9FA0A1A2A3A4A5A6A7A8A9AAABACADAEAFB0B1B2B3B4B5B6B7B8B9BABBBCBDBEBFC0C1C2C3C4C5C6C7C8C9CACBCCCDCECFD0D1D2D3D4D5D6D7D8D9DADBDCDDDEDFE0E1E2E3E4E5E6E7E8E9EAEBECEDEEEFF0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"
        // swiftlint:enable line_length
        if let data = input.hexadecimal {
            XCTAssert(data.count == 256)
            print("Input: \(input)")
            HexEditor.TextEncoding.allCases.forEach {
                let output = HexEditor.hexEditorString(data: data, textEncoding: $0)
                //XCTAssert(output.count == 256, "expected output.count == 256. Got \(output.count)")
                print("Ouput: \(output)")
            }
        }
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCharacterSets() {
        XCTAssert("0123456789ABCDEF".isHexString)
        XCTAssert(!"q".isHexString)
        XCTAssert(!" ".isHexString)
        XCTAssert("0".isHexString)
        XCTAssert("1".isHexString)
        XCTAssert("2".isHexString)
        XCTAssert("3".isHexString)
        XCTAssert("4".isHexString)
        XCTAssert("5".isHexString)
        XCTAssert("6".isHexString)
        XCTAssert("7".isHexString)
        XCTAssert("8".isHexString)
        XCTAssert("9".isHexString)
        XCTAssert("a".isHexString)
        XCTAssert("b".isHexString)
        XCTAssert("c".isHexString)
        XCTAssert("d".isHexString)
        XCTAssert("e".isHexString)
        XCTAssert("f".isHexString)
        XCTAssert("A".isHexString)
        XCTAssert("B".isHexString)
        XCTAssert("C".isHexString)
        XCTAssert("D".isHexString)
        XCTAssert("E".isHexString)
        XCTAssert("F".isHexString)
    }
    
    func testDecimalToOctalString() {
        let string = String(63, radix: 8, uppercase: false)
        XCTAssert(string == "77")
    }
    
    func testDecimalFromData() {
        do {
            guard var maxLengthData = "FFFFFFFFFFFFFFFF".hexadecimal else {
                XCTFail("Unrelated failure")
                return
            }
            XCTAssert(maxLengthData.pad(size: MemoryLayout<UInt64>.size))
            let u: UInt64 = maxLengthData.unsignedValue()
            XCTAssert(u > 0)
        }
//        do {
//            guard var tooLongData = "1FFFFFFFFFFFFFFFF".hexadecimal else {
//                XCTFail("Unrelated failure")
//                return
//            }
//            XCTAssert(!tooLongData.pad(size: MemoryLayout<UInt64>.size))
//            let u: UInt64 = tooLongData.unsignedValue()
//            XCTAssert(u > 0)
//        }
    }
}
