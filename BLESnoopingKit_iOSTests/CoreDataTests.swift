//
//  CoreDataTests.swift
//  BLESnoopingKitTests
//
//  Created by Zakk Hoyt on 4/8/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import XCTest
@testable import BLESnoopingKit_iOS

class CoreDataTests: XCTestCase {
    func testCoreDataCreate() {
        let name = "TestPeripheral"
        
        // Create a new entity
        let context = CoreDataStack.shared.mainContext
        let createExpectation = XCTestExpectation(description: "Creating Entity")
        CDPeripheral.create(
            context: context,
            configure: {
                $0.name = name
                $0.date = Date()
        }, completion: { result in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let entity):
                // Ensure name matches
                guard let entityName = entity.name else {
                    XCTFail("Expected entity.name == \(name). Got \(String(describing: entity.name))")
                    return
                }
                XCTAssert(entityName == name)
                
                // Retrieve the entity created above
                CDPeripheral.createOrUpdate(
                    context: context,
                    predicateProvider: { NSPredicate(format: "%K = %@", "name", name) },
                    completion: { result in
                        switch result {
                        case .success(let entity):
                            createExpectation.fulfill()
                            entity.deleteAndSave(context: context) { result in
                                switch result {
                                case .failure(let error):
                                    XCTFail(error.localizedDescription)
                                case .success:
                                    // We did it
                                    createExpectation.fulfill()
                                }
                            }
                        case .failure(let error):
                            XCTFail(error.localizedDescription)
                        }
                })
            }
        })
        
        wait(for: [createExpectation], timeout: 5.0)
    }
}
