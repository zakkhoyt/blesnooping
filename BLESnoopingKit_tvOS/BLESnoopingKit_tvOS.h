//
//  BLESnoopingKit_tvOS.h
//  BLESnoopingKit_tvOS
//
//  Created by Zakk Hoyt on 4/10/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BLESnoopingKit_tvOS.
FOUNDATION_EXPORT double BLESnoopingKit_tvOSVersionNumber;

//! Project version string for BLESnoopingKit_tvOS.
FOUNDATION_EXPORT const unsigned char BLESnoopingKit_tvOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BLESnoopingKit_tvOS/PublicHeader.h>


