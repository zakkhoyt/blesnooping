//
//  BLESnoopingKit.h
//  BLESnoopingKit
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BLESnoopingKit.
FOUNDATION_EXPORT double BLESnoopingKitVersionNumber;

//! Project version string for BLESnoopingKit.
FOUNDATION_EXPORT const unsigned char BLESnoopingKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BLESnoopingKit/PublicHeader.h>


