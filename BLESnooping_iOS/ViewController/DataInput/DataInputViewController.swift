//
//  DataInputViewController.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/8/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

typealias DataInputted = (Data?) -> Void

class DataInputViewController: UIViewController {
    // MARK: - Private data structures
    
    private enum InputType: Int, CaseIterable, CustomStringConvertible {
        case byteArray // hex input text field
        case byte // number input text field
        case bool // switch input
        case utf8 // text input text field
        case decimal // text input text field
        
        var description: String {
            switch self {
            case .byteArray: return "[UInt8]"
            case .byte: return "UInt8"
            case .bool: return "Bool"
            case .utf8: return "UTF-8"
            case .decimal: return "Decimal"
            }
        }
    }
    
    private enum WriteType: Int, CaseIterable, CustomStringConvertible {
        case command
        case request
        
        var description: String {
            switch self {
            case .command: return "Command"
            case .request: return "Request"
            }
        }
    }
    
    // MARK: - Internal vars
    
    var completion: DataInputted?
    
    // MARK: - Private vars
    
    private var inputType: InputType = .byteArray
    private var writeType: WriteType = .command
    
    // MARK: Each component in the stackView
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var inputTypeTitleLabel: UILabel!
    @IBOutlet weak var inputTypeSegment: UISegmentedControl!
    @IBOutlet weak var inputContainerView: UIView!
    @IBOutlet weak var inputPayloadCountLabel: UILabel!
    @IBOutlet weak var writeTypeTitleLabel: UILabel!
    @IBOutlet weak var writeTypeSegment: UISegmentedControl!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var writeButton: UIButton!

    // MARK: "modules" which act as child views in inputContainerView depending on inputType
    @IBOutlet var boolInputView: UIView!
    @IBOutlet weak var boolSwitch: SegmentSwitch!
    
    @IBOutlet var hexStringInputView: UIView!
    @IBOutlet weak var hexTextField: HexField!
    
    @IBOutlet var decimalInputView: UIView!
    @IBOutlet weak var decimalTextField: UITextField!
    
    @IBOutlet var textInputView: UIView!
    @IBOutlet weak var textField: UITextField!

    // MARK: - Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        func setupUI() {
            inputTypeSegment.removeAllSegments()
            InputType.allCases.forEach { inputTypeSegment.insertSegment(withTitle: $0.description, at: $0.rawValue, animated: false) }
            inputTypeSegment.selectedSegmentIndex = inputType.rawValue
            
            hexTextField.hexStringDelegate = self
            
            writeTypeSegment.removeAllSegments()
            WriteType.allCases.forEach { writeTypeSegment.insertSegment(withTitle: $0.description, at: $0.rawValue, animated: false) }
            writeTypeSegment.selectedSegmentIndex = writeType.rawValue
            
            setupInputTypeControl()
        }
        setupUI()
        updatePayload()
    }
    
    // MARK: - Private functions
    
    private func setupInputTypeControl() {
        let inputView: UIView = {
            switch inputType {
            case .byteArray: return hexStringInputView
            case .byte: return hexStringInputView
            case .bool: return boolInputView
            case .utf8: return textInputView
            case .decimal: return decimalInputView
            }
        }()
        
        // Remove what ever is already there
        inputContainerView.subviews.first?.removeFromSuperview()
        
        // Attach replacement
        inputView.translatesAutoresizingMaskIntoConstraints = false
        inputContainerView.addSubview(inputView)
        inputView.pinEdgesToParent()

        // Set text/value to reflect payload
        func setupInputControl() {
            let payload = self.payload
            switch inputType {
            case .byteArray: hexTextField.text = payload?.hexString
            case .byte: hexTextField.text = payload?.hexString
            case .bool:
                guard let currentPayload = payload else {
                    boolSwitch.isOn = false
                    return
                }
                boolSwitch.isOn = currentPayload.count == 0
                    ? false
                    : currentPayload.bytes[0] > 0
            case .utf8:
                guard let currentPayload = payload else {
                    hexTextField.text = nil
                    return
                }
                hexTextField.text = String(data: currentPayload, encoding: .utf8)
            case .decimal:
                guard var currentPayload = payload,
                    currentPayload.pad(size: MemoryLayout<UInt64>.size)else {
                        decimalTextField.text = nil
                        updatePayload()
                        return
                }
                
                let uint64: UInt64 = currentPayload.unsignedValue()
                decimalTextField.text = "\(uint64)"
            }
        }
        setupInputControl()
        
        func setupFirstResponder() {
            // Open the keyboard
            switch inputType {
            case .byteArray: hexTextField.becomeFirstResponder()
            case .byte: hexTextField.becomeFirstResponder()
            case .bool: boolSwitch.becomeFirstResponder()
            case .utf8: textField.becomeFirstResponder()
            case .decimal: decimalTextField.becomeFirstResponder()
            }
        }
        setupFirstResponder()
    }
    
    private var payload: Data?

    private func updatePayload() {
        payload = {
            switch inputType {
            case .byteArray: return hexTextField.text?.hexadecimal
            case .byte: return hexTextField.text?.hexadecimal
            case .bool: return Data([(boolSwitch.isOn ? 0x01 : 0x00)])
            case .utf8: return textField.text?.data(using: .utf8)
            case .decimal:
                guard let text = decimalTextField.text else { return nil }
                return Data.fromIntString(text)
            }
        }()
        
        inputPayloadCountLabel.text = "\((payload?.bytes ?? []).count) bytes"
    }
    
    private func write() {
        if let payload = payload {
            Logger.debug("DataInput", "Payload: \(payload.hexString)")
        } else {
            Logger.debug("DataInput", "Payload: -")
        }
        
        // Call completion handler with input. It is owner's reponsibilty to dismiss.
        if let completion = completion {
            completion(payload)
        } else {
            (navigationController ?? self).dismiss(animated: true, completion: nil)
        }
    }

    // MARK: IBActions
    
    @IBAction
    func inputTypeSegmentValueChanged(_ sender: UISegmentedControl) {
        guard let inputType = InputType(rawValue: sender.selectedSegmentIndex) else {
            preconditionFailure()
        }
        self.inputType = inputType
        setupInputTypeControl()
    }
    
    @IBAction
    func writeTypeSegmentValueChanged(_ sender: Any) {
    }
    
    @IBAction
    func textFieldEditingChanged(_ sender: Any) {
        updatePayload()
    }
    
    @IBAction
    func cancelButtonTouchUpInside(_ sender: Any) {
        // Call completion handler with nil. It is owner's reponsibilty to dismiss.
        if let completion = completion {
            completion(nil)
        } else {
            (navigationController ?? self).dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction
    func writeButtonTouchUpInside(_ sender: Any) {
        write()
    }
    
    @IBAction
    func boolSwitchValueChanged(sender: SegmentSwitch) {
        updatePayload()
    }
}

extension DataInputViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        switch textField {
        case hexTextField: return string.isHexString
        case textField: return true
        case decimalTextField: return string.isDecimalString
        default: return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        updatePayload()
        write()
        return true
    }
}

extension DataInputViewController: HexFieldHexStringDelegate {
    func hexField(_ hexField: HexField, didUpdateHexString text: String?) {
        updatePayload()
    }
}

extension DataInputViewController: StoryboardInitializable {
    static var storyboardName: String = "DataInputViewController"
    static var storyboardIdentifier: String? = nil
}
