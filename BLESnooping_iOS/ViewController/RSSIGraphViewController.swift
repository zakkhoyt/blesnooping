//
//  RSSIGraphViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/3/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
import Charts

#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class RSSIGraphViewController: UIViewController {
    // MARK: Internal vars
    
    var updatedDevices: [BLEDevice] = [] {
        didSet {
            // Only copy devices which are in our original list (passed via init)
            let devices: [BLEDevice] = updatedDevices.filter { self.devices.contains($0) }
            self.devices = devices
        }
    }
    
    // MARK: Private vars
    
    private let chartView = LineChartView()
    private var devices: [BLEDevice] {
        didSet {
            reloadChart()
        }
    }
    
    // MARK: Inits
    
    init(devices: [BLEDevice]) {
        self.devices = devices
        
        super.init(nibName: nil, bundle: nil)
        //device.rssiDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        reloadChart()
    }
    
    // MARK: Private functions
    
    private func setupUI() {
        #if os(iOS)
            view.backgroundColor = .systemBackground
        #else
            view.backgroundColor = .darkGray
        #endif

        navigationItem.title = "RSSI Chart"

        chartView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(chartView)
        chartView.pinEdgesToParent()
        chartView.delegate = self
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = true
        
        let l = chartView.legend
        l.horizontalAlignment = .right
        l.verticalAlignment = .top
        l.orientation = .vertical
        l.drawInside = false
        l.font = .preferredFont(forTextStyle: .caption1)
        l.yOffset = 8
        l.textColor = UIColor.systemGreen

        let leftAxis = chartView.leftAxis
        leftAxis.labelFont = .preferredFont(forTextStyle: .caption1)
        leftAxis.axisMaximum = 0
        leftAxis.axisMinimum = -120
        leftAxis.labelTextColor = .systemRed
        leftAxis.inverted = false
        leftAxis.gridLineWidth = 0.5
        leftAxis.axisLineWidth = 2
        chartView.rightAxis.enabled = false
        
        let xAxis = chartView.xAxis
        xAxis.labelFont = .preferredFont(forTextStyle: .caption1)
        xAxis.labelTextColor = .systemGreen
        xAxis.gridLineWidth = 0.5
        xAxis.axisLineWidth = 2
        xAxis.axisMinimum = 0
    }
    
    private func reloadChart() {
        // Find the date longest ago
        var biggest: Date = Date(timeIntervalSince1970: 0)
        for device in devices {
            for tuple in device.rssis {
                if tuple.date > biggest {
                    biggest = tuple.date
                }
            }
        }

        let dataSets: [LineChartDataSet] = devices.compactMap { device in
            // X needs to be increasing order
            let dataEntries: [ChartDataEntry] = device.rssis.reversed().compactMap {
                let elapsed = biggest.timeIntervalSince($0.date)
                return ChartDataEntry(x: Double(abs(elapsed)), y: Double($0.rssi))
            }
            
            let dataSet = LineChartDataSet(entries: dataEntries, label: device.name ?? kUnavailable)
            dataSet.axisDependency = .left
            dataSet.setColor(device.color.withAlphaComponent(0.25))
            dataSet.setCircleColor(device.color.withAlphaComponent(0.5))
            dataSet.lineWidth = 2
            dataSet.circleRadius = 3
            dataSet.fillAlpha = 65/255
            dataSet.fillColor = device.color
            dataSet.highlightColor = device.color
            dataSet.drawCircleHoleEnabled = false
            dataSet.valueTextColor = device.color
            dataSet.valueFont = UIFont.preferredFont(forTextStyle: .caption2)
            
            return dataSet
        }

        let data = LineChartData(dataSets: dataSets)
        data.setValueFont(.preferredFont(forTextStyle: .caption2))

        chartView.data = data
    }
}

extension RSSIGraphViewController: BLEDeviceRSSIDelegate {
    func bleDevice(_ device: BLEDevice, didUpdateRSSIs rssis: [RSSITuple]) {
        reloadChart()
    }
}

extension RSSIGraphViewController: ChartViewDelegate {
}
