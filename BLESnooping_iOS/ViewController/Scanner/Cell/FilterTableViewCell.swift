//
//  FilterTableViewCell.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/7/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class FilterTableViewCell: UITableViewCell {
}

extension FilterTableViewCell: TableViewReusable {
    static var reuseIdentifier: String {
        return "FilterTableViewCell"
    }
}
