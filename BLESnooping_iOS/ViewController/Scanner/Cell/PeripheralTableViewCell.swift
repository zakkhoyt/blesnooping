//
//  PeripheralTableViewCell.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class PeripheralTableViewCell: UITableViewCell {
    var connectButtonTapped: (() -> Void)?
    
    @IBOutlet weak var deviceImageView: UIImageView!
    @IBOutlet weak var rssiView: RSSIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    @IBOutlet weak var isConnectableLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var manufacturerDataLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var serviceDataLabel: UILabel!
    @IBOutlet weak var overflowServiceUUIDsLabel: UILabel!
    @IBOutlet weak var solicitedServiceUUIDsLabel: UILabel!
    
    @IBOutlet weak var mtuLabel: UILabel!
    @IBOutlet weak var txPowerLabel: UILabel!
    
    @IBOutlet weak var elapsedLabel: UILabel!
    @IBOutlet weak var rssiLabel: UILabel!
    
    @IBOutlet weak var connectButton: UIButton!

    //override var canBecomeFocused: Bool { false }
    
    @IBAction
    func connectButtonTouchUpInside(_ sender: Any) {
        connectButtonTapped?()
    }
}

extension PeripheralTableViewCell: TableViewReusable {
    static var reuseIdentifier: String {
        return "PeripheralTableViewCell"
    }
}
