//
//  ViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
import CoreBluetooth
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class ScannerViewController: BaseViewController {
    // MARK: Private vars
    
    private lazy var bleScanner: BLEScanner = BLEReconnectScanner(delegate: self)
    
    private var filteredOutDevices: [BLEDevice] = []
    private var devices: [BLEDevice] = [] {
        didSet {
            if let rssiViewController = navigationController?.viewControllers.last as? RSSIGraphViewController {
                // Update the graph if present
                rssiViewController.updatedDevices = devices
            }
            
            // We can either:
            //   1.) reloadData (no animation)
            //   2.) try to animate them.
            
            // 1.)
            tableView.reloadData()
            return
            
            // 2.)
            
            // Notes: The code below here does a good job of animating new devices into the table view,
            // but has a few issues:
            // a.) Contenxt menus flicker while open because the cell keeps being reused for other devices
            // b.) The table jumps around due to inconsistent table view heights.
            
            //            // insert, delete, or move
            //            let previousDevices = oldValue
            //            let nextDevices = devices
            //
            //            let previousDevicesSet = Set<BLEDevice>(previousDevices)
            //            let nextDevicesSet = Set<BLEDevice>(nextDevices)
            //
            //            let toInsert: Set<BLEDevice> = nextDevicesSet.subtracting(previousDevicesSet)
            //            let toDelete: Set<BLEDevice> = previousDevicesSet.subtracting(nextDevicesSet)
            //            let toMove: Set<BLEDevice> = previousDevicesSet.intersection(nextDevicesSet)
            //
            //            tableView.beginUpdates()
            //
            //            // move or mark to redraw (after updates block
            //            var indexPathsToRedraw: [IndexPath] = []
            //            for device in toMove {
            //                guard let fromIndex = previousDevices.firstIndex(of: device),
            //                    let toIndex = nextDevices.firstIndex(of: device) else {
            //                        // Can't find it... probably an error but shouldn't happen
            //                        continue
            //                }
            //
            //                // If we aren't moving it, then let's redraw it
            //                guard fromIndex != toIndex else {
            //                    let indexPath = IndexPath(row: fromIndex, section: TableSection.peripherals.rawValue)
            //                    indexPathsToRedraw.append(indexPath)
            //                    continue
            //                }
            //                let fromIndexPath = IndexPath(row: fromIndex, section: TableSection.peripherals.rawValue)
            //                let toIndexPath = IndexPath(row: toIndex, section: TableSection.peripherals.rawValue)
            //                tableView.moveRow(at: fromIndexPath, to: toIndexPath)
            //            }
            //
            //            // insert
            //            var indexPathsToInsert: [IndexPath] = []
            //            for device in toInsert {
            //                guard let index = nextDevices.firstIndex(of: device) else { continue }
            //                let indexPath = IndexPath(row: index, section: TableSection.peripherals.rawValue)
            //                indexPathsToInsert.append(indexPath)
            //            }
            //            tableView.insertRows(at: indexPathsToInsert, with: .automatic)
            //
            //            // delete
            //            var indexPathsToDelete: [IndexPath] = []
            //            for device in toDelete {
            //                guard let index = previousDevices.firstIndex(of: device) else { continue }
            //                let indexPath = IndexPath(row: index, section: TableSection.peripherals.rawValue)
            //                indexPathsToDelete.append(indexPath)
            //            }
            //            tableView.deleteRows(at: indexPathsToDelete, with: .automatic)
            //
            //            tableView.endUpdates()
            //
            //            tableView.reloadRows(at: indexPathsToRedraw, with: .none)
        }
    }
    
    private var filter = PublicUserDefaults.readFilter() ?? BLEFilter() {
        didSet {
            PublicUserDefaults.writeFilter(filter)
            tableView.reloadData()
        }
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var scanBarb: UIBarButtonItem!
    @IBOutlet var tableView: UITableView!
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Devices"
        
        let filterBarb = UIBarButtonItem(title: "Filter", style: .plain, target: self, action: #selector(filterBarbAction))
        let graphBarb = UIBarButtonItem(title: "Graph", style: .plain, target: self, action: #selector(graphBarbAction))
        
        navigationItem.leftBarButtonItems = [filterBarb, graphBarb]
        
        setupTableView()
        updateScanBarb()
        
        setupNotificationObservers()
        // Do any additional setup after loading the view
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        verifyBLEPermission()

//        #if DEBUG
//        let vc = DataInputViewController.loadFromStoryboard()
//        vc.modalTransitionStyle = .crossDissolve
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
//        #endif
    }
    
    // MARK: Private functions
    private func setupNotificationObservers() {
        // No need to store return value because we are root VC and will never be deallocated.
        _ = NotificationCenter.default.addObserver(
            forName: UIApplication.didBecomeActiveNotification,
            object: self,
            queue: .main
        ) { note in
            self.displayTableView()
            self.verifyBLEPermission()
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        FilterHeaderView.register(to: tableView)
        FilterTableViewCell.register(to: tableView)
        PeripheralTableViewCell.register(to: tableView)
        tableView.tableFooterView = UIView()
        
        #if os(iOS)
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refreshAction), for: .valueChanged)
        tableView.refreshControl = refresh
        #endif
    }
    
    private func startScanner() {
        bleScanner.resetDevices()
        devices.removeAll()
        tableView.reloadData()
        
        bleScanner.startScanning()
        updateScanBarb()
    }
    
    private func stopScanner() {
        bleScanner.stopScanning()
        updateScanBarb()
    }
    
    private func updateScanBarb() {
        scanBarb.title = {
            switch bleScanner.isScanning {
            case true: return "Stop Scan"
            case false: return "Start Scan"
            }
        }()
    }
    
    private func reloadCell(for device: BLEDevice) {
        guard let index = devices.firstIndex(of: device) else { return }
        let indexPath = IndexPath(row: index, section: 0)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    private func presentBLEError(title: String,
                                 message: String,
                                 buttonTitle: String?) {
        guard self.presentedViewController == nil else {
            Logger.debug("BLE Problem", "Already presenting a problem VC")
            return
        }
        
        let problem = BLEProblemViewController.Problem(title: title, message: message, buttonTitle: buttonTitle)
        let vc = BLEProblemViewController.loadFromStoryboard {
            BLEProblemViewController(coder: $0, problem: problem)
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        navigationController?.present(vc, animated: true, completion: nil)
    }
    
    private func displayTableView() {
        // Remove BLEProblemView if presented
        if let vc = presentedViewController as? BLEProblemViewController {
            vc.dismiss(animated: true, completion: nil)
        }
        
        guard tableView.superview == nil else { return }
        view.addSubview(tableView)
        tableView.pinEdgesToParent(useSafeAreaLayoutGuide: true)
        tableView.reloadData()
    }
    
    private func verifyBLEPermission() {
        if #available(iOS 13.1, *) {
        } else {
            switch CBCentralManager.authorization {
            case .notDetermined:
                break
            case .restricted:
                presentBLEError(title: "BLE Access is Restricted",
                                message: "Please enable BLE permission continue.",
                                buttonTitle: "Bluetooth Privacy")
            case .denied:
                presentBLEError(title: "BLE Access is Denied",
                                message: "Please enable BLE permission continue.",
                                buttonTitle: "Bluetooth Privacy")
            case .allowedAlways:
                displayTableView()
            @unknown default:
                // Be optimistic that new enum value will be some enabled value
                displayTableView()
            }
        }
    }
    private func navigateToRSSIGraphViewController(devices: [BLEDevice]) {
        let vc = RSSIGraphViewController(devices: devices)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    private func navigateToFilterViewController() {
        let vc = FilterViewController(filter: filter, delegate: self)
        let nc = UINavigationController(rootViewController: vc)
        nc.transitioningDelegate = vc.animationCoordinator
        nc.modalPresentationStyle = .custom
        present(nc, animated: true, completion: nil)
    }
    
    // MARK: IBActions
    @IBSegueAction
    func createDeviceViewController(coder: NSCoder, sender: Any?, segueIdentifier: String?) -> DeviceViewController? {
        let device = sender as! BLEDevice
        return DeviceViewController(coder: coder, device: device)
    }

    @IBAction
    func scanBarbAction(_ sender: Any) {
        switch bleScanner.isScanning {
        case true: return stopScanner()
        case false: return startScanner()
        }
    }
    
    #if os(iOS)
    @objc
    private func refreshAction(sender: UIRefreshControl) {
        startScanner()
        sender.endRefreshing()
    }
    #endif
    
    @objc
    private func filterBarbAction(sender: UIBarButtonItem) {
        navigateToFilterViewController()
    }

    @objc
    private func graphBarbAction(sender: UIBarButtonItem) {
        navigateToRSSIGraphViewController(devices: devices)
    }
}

extension ScannerViewController {
    enum TableSection: Int, CaseIterable {
        case peripherals
    }
}

extension ScannerViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tableSection = TableSection(rawValue: section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals: return devices.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        func configurePeriheralCell(indexPath: IndexPath) -> PeripheralTableViewCell {
            // Show each BLE device and some of its status.
            let cell = tableView.dequeueReusableCell(withIdentifier: PeripheralTableViewCell.reuseIdentifier, for: indexPath) as! PeripheralTableViewCell
            let device = devices[indexPath.row]
            
            cell.deviceImageView.image = UIImage(named: "ble_44x44")?.withTintColor(device.color)
            
            cell.connectButton.setTitle(device.connectButtonTitle, for: .normal)
            cell.connectButtonTapped = { [weak self] in
                guard let safeSelf = self else { return }
                // TODO: This is repeated at didSelect. Refactor
                let device = safeSelf.devices[indexPath.row]
                switch device.peripheral.state {
                case .disconnected:
                    safeSelf.bleScanner.stopScanning()
                    safeSelf.bleScanner.connect(to: device)
                case .connected:
                    safeSelf.bleScanner.disconnect(from: device)
                default: break
                }
            }
            
            if let advertisedIsConnectable = device.isConnectable {
                cell.isConnectableLabel.text = "Connectable: \(advertisedIsConnectable)"
            } else {
                cell.isConnectableLabel.text = nil
            }
            
            cell.stateLabel.text = device.peripheral.state.description
            
            cell.nameLabel.setTextOrUnavailable(device.name)
            
            cell.uuidLabel.text = device.peripheral.identifier.uuidString
            if let advertisedManufacturerData = device.advertisedManufacturerDataString {
                cell.manufacturerDataLabel.text = "Manufacterer Data: \(advertisedManufacturerData)"
            } else {
                cell.manufacturerDataLabel.text = nil
            }
            
            if let advertisedServicesString = device.advertisedServicesString {
                cell.servicesLabel.text = "Services: \(advertisedServicesString)"
            } else {
                cell.servicesLabel.text = nil
            }
            
            if let advertisedServicesDataString = device.advertisedServicesDataString {
                cell.serviceDataLabel.text = "Service Data: \(advertisedServicesDataString)"
            } else {
                cell.serviceDataLabel.text = nil
            }
            
            if let advertisedOverflowServiceUUIDsString = device.advertisedOverflowServiceUUIDsString {
                cell.overflowServiceUUIDsLabel.text = "Overflow Service UUIDs: \(advertisedOverflowServiceUUIDsString)"
            } else {
                cell.overflowServiceUUIDsLabel.text = nil
            }
            
            if let advertisedSolicitedServiceUUIDsString = device.advertisedSolicitedServiceUUIDsString {
                cell.solicitedServiceUUIDsLabel.text = "Solicited Service UUIDs: \(advertisedSolicitedServiceUUIDsString)"
            } else {
                cell.solicitedServiceUUIDsLabel.text = nil
            }
            
            if let advertisedTXPowerLevel = device.txPowerLevel {
                cell.txPowerLabel.text = "TX Power: \(advertisedTXPowerLevel) dBm"
            } else {
                cell.txPowerLabel.text = nil
            }
            
            cell.mtuLabel.text = nil
            
            cell.rssiLabel.text = "RSSI: \(device.rssi) dBm"
            
            cell.elapsedLabel.text = device.elapsedSinceRssi
            
            cell.rssiView.rssi = device.rssi
            return cell
        }
        
        guard let tableSection = TableSection(rawValue: indexPath.section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals: return configurePeriheralCell(indexPath: indexPath)
        }
    }
}

extension ScannerViewController {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let tableSection = TableSection(rawValue: section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals:
            switch filter.isEnabled {
            case true:
                guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: FilterHeaderView.reuseIdentifier) as? FilterHeaderView else {
                    preconditionFailure()
                }
                headerView.filterCountLabel.text = {
                    guard filter.isEnabled else { return nil }
                    guard devices.count > 0 else { return nil }
                    return "(hiding \(filteredOutDevices.count) / \(devices.count))"
                }()
                headerView.tapped = { [weak self] in
                    self?.navigateToFilterViewController()
                }
                return headerView
            case false:
                return nil
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        guard let tableSection = TableSection(rawValue: section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals: return filter.isEnabled ? 64 : 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let tableSection = TableSection(rawValue: section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals: return filter.isEnabled ? UITableView.automaticDimension : 0
        }
    }
}

extension ScannerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let tableSection = TableSection(rawValue: indexPath.section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals: return 120
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}

extension ScannerViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableSection = TableSection(rawValue: indexPath.section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals:
            tableView.deselectRow(at: indexPath, animated: false)
            let device = devices[indexPath.row]
            switch device.peripheral.state {
            case .disconnected:
                bleScanner.stopScanning()
                bleScanner.connect(to: device)
            case .connected:
                bleScanner.disconnect(from: device)
            default: break
            }
        }
    }
}

#if os(iOS)
extension ScannerViewController {
    func tableView(_ tableView: UITableView,
                   contextMenuConfigurationForRowAt indexPath: IndexPath,
                   point: CGPoint) -> UIContextMenuConfiguration? {
        guard let tableSection = TableSection(rawValue: indexPath.section) else { preconditionFailure() }
        switch tableSection {
        case .peripherals:
            let device = devices[indexPath.row]
            return UIContextMenuConfiguration(identifier: device.peripheral.identifier.uuidString as NSString, previewProvider: nil) { _ in
                let connectAction: UIAction = {
                    switch device.peripheral.state {
                    case .disconnected:
                        return UIAction(title: device.connectButtonTitle,
                                        image: UIImage(systemName: "radiowaves.right")) { [weak self] _ in
                                            guard let safeSelf = self else { return }
                                            safeSelf.bleScanner.connect(to: device)
                        }
                    default:
                        return UIAction(title: device.connectButtonTitle,
                                        image: UIImage(systemName: "radiowaves.left")) { [weak self] _ in
                                            guard let safeSelf = self else { return }
                                            safeSelf.bleScanner.disconnect(from: device)
                        }
                    }
                }()
                
                let rssiGraphAction = UIAction(
                    title: "RSSI Graph",
                    image: UIImage(systemName: "chart.bar.fill")) { [weak self] _ in
                        guard let safeSelf = self else { return }
                        safeSelf.navigateToRSSIGraphViewController(devices: [device])
                }
                
                return UIMenu(title: "", children: [connectAction, rssiGraphAction])
            }
        }
    }
    
    /// This is fired when the user taps on the preview view itself
    func tableView(_ tableView: UITableView,
                   willPerformPreviewActionForMenuWith configuration: UIContextMenuConfiguration,
                   animator: UIContextMenuInteractionCommitAnimating) {
        guard let identifier = (configuration.identifier as? NSString) as String? else { return }
        guard let device = (devices.first { $0.peripheral.identifier.uuidString == identifier }) else { return }
        animator.addCompletion {
            self.navigateToRSSIGraphViewController(devices: [device])
        }
    }
    
    //    func tableView(_ tableView: UITableView,
    //                   previewForHighlightingContextMenuWithConfiguration configuration: UIContextMenuConfiguration) -> UITargetedPreview? {
    //        guard let identifier = configuration.identifier as? String else { return nil }
    //        guard let index = (devices.firstIndex(where: { $0.peripheral.identifier.uuidString == identifier })) else { return nil }
    //        guard let cell = tableView.cellForRow(at: IndexPath(row: index,
    //                                                            section: TableSection.peripherals.rawValue)) as? PeripheralTableViewCell else { return nil }
    //        //return UITargetedPreview(view: cell)
    //
    //        let pView = UIView()
    //        pView.alpha = 0
    //        pView.bounds = CGRect(x: 0, y: 0, width: cell.frame.width, height: cell.frame.height)
    //        view.addSubview(pView)
    //        pView.backgroundColor = UIColor.brown
    //
    //        guard let image = UIImage(view: cell) else { return nil }
    //        let imageView = UIImageView(image: image)
    //        imageView.translatesAutoresizingMaskIntoConstraints = false
    //
    //        pView.addSubview(imageView)
    //        imageView.leadingAnchor.constraint(equalTo: pView.leadingAnchor).isActive = true
    //        imageView.topAnchor.constraint(equalTo: pView.topAnchor).isActive = true
    //        imageView.trailingAnchor.constraint(equalTo: pView.trailingAnchor).isActive = true
    //        pView.layoutIfNeeded()
    //        return UITargetedPreview(view: pView)
    //    }
}
#endif

extension ScannerViewController: BLEScannerDelegate {
    func bleScanner(_ bleScanner: BLEScanner, didUpdateState state: CBManagerState) {
        switch state {
        case .unknown: break
        case .resetting: break
        case .unsupported:
            presentBLEError(title: "BLE is Unsupported",
                            message: "This device does not support BlueTooth Low Energy (BLE)",
                            buttonTitle: nil)
        case .unauthorized:
            presentBLEError(title: "BLE Access is Unauthorized",
                            message: "Please enable BLE permission continue.",
                            buttonTitle: "Bluetooth Privacy")
        case .poweredOff:
            let message = """
            Please turn Bluetooth to continue.
            """
            presentBLEError(title: "BLE is powered off",
                                message: message,
                                buttonTitle: nil)
        case .poweredOn: displayTableView()
        @unknown default: break
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDesiredScanState scanState: BLEBasicScanner.ScanState) {
        DispatchQueue.main.async {
            self.updateScanBarb()
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDevices devices: [BLEDevice]) {
        //let sortedDevices: [BLEDevice] = [BLEDevice](devices).sorted { $0.peripheral.identifier.uuidString < $1.peripheral.identifier.uuidString }
        let sortedDevices: [BLEDevice] = [BLEDevice](devices)
            .filter { !filter.shouldFilterOut(device: $0) }
            .sorted { $0.createdDate < $1.createdDate }
        
        let filteredOutDevices = [BLEDevice](devices)
            .filter { filter.shouldFilterOut(device: $0) }
        
        DispatchQueue.main.async {
            self.devices = sortedDevices
            self.filteredOutDevices = filteredOutDevices
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didConnectTo device: BLEDevice) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
            let vc = DeviceViewController.loadFromStoryboard { DeviceViewController(coder: $0, device: device) }
            let nc = UINavigationController(rootViewController: vc)
            self.present(nc, animated: true, completion: nil)
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didFailToConnectTo device: BLEDevice) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didDisconnectFrom device: BLEDevice, isExpected: Bool) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
        }
    }
}

extension ScannerViewController: FilterViewControllerDelegate {
    func filterViewController(_ filterViewController: FilterViewController, didUpdateFilter filter: BLEFilter) {
        filterViewController.dismiss(animated: true, completion: nil)
        self.filter = filter
    }
}

extension UIImage {
    convenience init?(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}
