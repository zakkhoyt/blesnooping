//
//  FilterHeaderView.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/7/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class FilterHeaderView: UITableViewHeaderFooterView {
    var tapped: (() -> Void)?
    @IBOutlet weak var filterTitleLabel: UILabel!
    @IBOutlet weak var filterCountLabel: UILabel!
    @IBOutlet weak var tintedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tintedView.backgroundColor = UIColor.systemRed.withAlphaComponent(0.5)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        tapped?()
    }
}

extension FilterHeaderView: TableViewReusable {
    static var reuseIdentifier: String {
        return "FilterHeaderView"
    }
}
