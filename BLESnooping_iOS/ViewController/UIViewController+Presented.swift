//
//  UIViewController+Presented.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/9/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit

extension UIViewController {
    var mostPresentedViewController: UIViewController {
        var topController = self
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    }
    
    func dismissAll(animated: Bool = false, completion: (() -> Void)? = nil) {
        // This dismisses everything above root view controller. No matter how many modals presenting modals
        self.dismiss(animated: animated) {
            completion?()
        }
    }
}
