//
//  ViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
import CoreBluetooth

private let kSegueScannerToDevice = "kSegueScannerToDevice"

class ScannerViewController: BaseViewController {
    // MARK: Private vars
    
    // swiftlint:disable implicitly_unwrapped_optional
    var bleScanner: BLEScanner!
    // swiftlint:enable implicitly_unwrapped_optional
    
    var devices: [BLEDevice] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    // MARK: IBOutlets
    
    @IBOutlet weak var scanBarb: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Overrides
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bleScanner = BLEScanner(delegate: self)
        setupTableView()
        updateScanBarb()
        
        // Do any additional setup after loading the view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kSegueScannerToDevice {
            let vc = segue.destination as! DeviceViewController
            let device = sender as! BLEDevice
            vc.device = device
        }
    }

    // MARK: Private functions

    private func setupTableView() {
        PeripheralTableViewCell.register(to: tableView)
        tableView.tableFooterView = UIView()
    }
    
    private func startScanner() {
        bleScanner.startScanning()
        updateScanBarb()
    }

    private func stopScanner() {
        bleScanner.stopScanning()
        updateScanBarb()
    }

    private func updateScanBarb() {
        scanBarb.title = {
            switch bleScanner.isScanning {
            case true: return "Stop Scan"
            case false: return "Start Scan"
            }
        }()
    }
    
    private func reloadCell(for device: BLEDevice) {
        guard let index = devices.firstIndex(of: device) else { return }
        let indexPath = IndexPath(row: index, section: 0)
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }

    // MARK: IBActions
    
    @IBAction
    func scanBarbAction(_ sender: Any) {
        switch bleScanner.isScanning {
        case true: return stopScanner()
        case false: return startScanner()
        }
    }
}

extension ScannerViewController {
    enum TableSection: Int, CaseIterable {
        case peripherals
    }
}

extension ScannerViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return TableSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PeripheralTableViewCell.reuseIdentifier, for: indexPath) as! PeripheralTableViewCell
        let device = devices[indexPath.row]
        cell.identifierLabel.text = device.peripheral.identifier.uuidString
        cell.nameLabel.text = device.name
        cell.stateLabel.text = device.peripheral.state.description
        cell.initializedLabel.text = device.isInitialized ? "initialized" : "uninitialized"
        return cell
    }
}

extension ScannerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let device = devices[indexPath.row]
        switch device.peripheral.state {
        case .disconnected:
            bleScanner.stopScanning()
            bleScanner.connect(to: device)
        case .connected:
            bleScanner.disconnect(from: device)
        default: break
        }
    }
}

extension ScannerViewController: BLEScannerDelegate {
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDesiredScanState scanState: BLEScanner.ScanState) {
        DispatchQueue.main.async {
            self.updateScanBarb()
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didUpdateDevices devices: Set<BLEDevice>) {
        let sortedDevices: [BLEDevice] = [BLEDevice](devices).sorted { $0.peripheral.identifier.uuidString < $1.peripheral.identifier.uuidString }
        DispatchQueue.main.async {
            self.devices = sortedDevices
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didConnectTo device: BLEDevice) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
            device.initialize(delegate: self)
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didFailToConnectTo device: BLEDevice) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
        }
    }
    
    func bleScanner(_ bleScanner: BLEScanner, didDisconnectFrom device: BLEDevice, isExpected: Bool) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
            if !isExpected {
                let seconds: TimeInterval? = {
                    guard let initializedDate = device.initializedDate else { return nil }
                    return Date().timeIntervalSince(initializedDate)
                }()
                let secondsString: String = {
                    guard let seconds = seconds else { return "-" }
                    return "\(seconds) seconds"
                }()
                self.alert(title: "Error",
                           message: "Unexpectely disconnected from \(device.name) after \(secondsString)")
            }
        }
    }
}

extension ScannerViewController: BLEDeviceDelegate {
    func bleDeviceDidInitialize(_ device: BLEDevice) {
        DispatchQueue.main.async {
            self.reloadCell(for: device)
            self.performSegue(withIdentifier: kSegueScannerToDevice, sender: device)
        }
    }
}
