//
//  FilterViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/2/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

protocol FilterViewControllerDelegate: AnyObject {
    func filterViewController(_ filterViewController: FilterViewController,
                              didUpdateFilter filter: BLEFilter)
}

class FilterViewController: UIViewController {
    private let stackView = UIStackView()
    // swiftlint:disable implicitly_unwrapped_optional
    private weak var delegate: FilterViewControllerDelegate!
    // swiftlint:enable implicitly_unwrapped_optional
    private var filter: BLEFilter {
        didSet {
           updateUI()
        }
    }
    
    let animationCoordinator = PromptTransitionCoordinator()
    
    let isEnabledCell = TextFieldCell(isHex: false)
    let nameFilterCell = TextFieldCell(isHex: false)
    let dataFilterCell = TextFieldCell(isHex: true)
    let rssiFilterCell = StepperCell()
    
    init(filter: BLEFilter, delegate: FilterViewControllerDelegate) {
        self.filter = filter
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        preconditionFailure("Not implemented. Use init(BLEFilter:FilterViewControllerDelegate)")
    }
    
    private func commonInit() {
        navigationItem.title = "Filter"
        
        let doneBarb = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneBarbAction))
        navigationItem.rightBarButtonItem = doneBarb
        
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        view.addSubview(stackView)
        let kInset: CGFloat = 16
        stackView.pinEdgesToParent(insets: UIEdgeInsets(top: kInset, left: kInset, bottom: kInset, right: kInset))
        
        self.transitioningDelegate = animationCoordinator
        self.modalPresentationStyle = .custom
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        updateUI()
    }
    
    private func setupUI() {
        #if os(iOS)
            view.backgroundColor = .systemBackground
        #else
            view.backgroundColor = .darkGray
        #endif
        
        addIsEnabledTextFieldCell()
        addNameTextFieldCell()
        addDataTextFieldCell()
        addRSSICell()
        
        // Takes up the dead space between the bottom cell and the bottom of the view.
        let view = UIView()
        view.backgroundColor = .clear
        stackView.addArrangedSubview(view)
    }

    private func updateUI() {
        isEnabledCell.toggleSwitch.isOn = filter.isEnabled
        
        nameFilterCell.textField.text = filter.name.compareTo
        nameFilterCell.toggleSwitch.isOn = filter.name.isEnabled
        
        dataFilterCell.textField.text = filter.data.compareTo
        dataFilterCell.toggleSwitch.isOn = filter.data.isEnabled
        
        rssiFilterCell.stepperValueLabel.text = "\(Int(filter.rssiGreaterThan.compareTo))"
        rssiFilterCell.stepper.value = Double(filter.rssiGreaterThan.compareTo)
        rssiFilterCell.toggleSwitch.isOn = filter.rssiGreaterThan.isEnabled
    }

    private func addIsEnabledTextFieldCell() {
        isEnabledCell.label.text = "Enabled"
        isEnabledCell.textField.alpha = 0.0001 // If we hide this, layout will change
        isEnabledCell.toggleSwitch.addTarget(self, action: #selector(toggleSwitchValueChanged), for: .valueChanged)
        isEnabledCell.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(isEnabledCell)
    }

    private func addNameTextFieldCell() {
        nameFilterCell.label.text = "Name"
        nameFilterCell.textField.delegate = self
        nameFilterCell.textField.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        nameFilterCell.toggleSwitch.addTarget(self, action: #selector(toggleSwitchValueChanged), for: .valueChanged)
        nameFilterCell.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(nameFilterCell)
    }
    
    private func addDataTextFieldCell() {
        dataFilterCell.label.text = "Data"
        dataFilterCell.textField.delegate = self
        dataFilterCell.textField.addTarget(self, action: #selector(textFieldEditingChanged), for: .editingChanged)
        dataFilterCell.toggleSwitch.addTarget(self, action: #selector(toggleSwitchValueChanged), for: .valueChanged)
        dataFilterCell.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubview(dataFilterCell)
    }
    
    private func addRSSICell() {
        rssiFilterCell.label.text = "RSSI"
        rssiFilterCell.stepperValueLabel.text = "\(Int(filter.rssiGreaterThan.compareTo))"
//        rssiFilterCell.stepper.value = Double(filter.rssiGreaterThan.compareTo)
        rssiFilterCell.stepper.addTarget(self, action: #selector(rssiStepperValueChanged), for: .valueChanged)
        rssiFilterCell.toggleSwitch.addTarget(self, action: #selector(toggleSwitchValueChanged), for: .valueChanged)
        stackView.addArrangedSubview(rssiFilterCell)
    }

    // MARK: IBActions
    @objc
    private func doneBarbAction(sender: UIBarButtonItem) {
        delegate.filterViewController(self, didUpdateFilter: filter)
    }
    
    @objc
    private func textFieldEditingChanged(sender: UITextField) {
        switch sender {
        case nameFilterCell.textField: filter.name.compareTo = sender.text ?? ""
        case dataFilterCell.textField: filter.data.compareTo = sender.text ?? ""
        default:
            break
        }
    }
    
    @objc
    private func rssiStepperValueChanged(sender: UIStepper) {
        filter.rssiGreaterThan.compareTo = Float(sender.value)
    }

    @objc
    private func toggleSwitchValueChanged(sender: SegmentSwitch) {
        switch sender {
        case isEnabledCell.toggleSwitch: filter.isEnabled = sender.isOn
        case nameFilterCell.toggleSwitch: filter.name.isEnabled = sender.isOn
        case dataFilterCell.toggleSwitch: filter.data.isEnabled = sender.isOn
        case rssiFilterCell.toggleSwitch: filter.rssiGreaterThan.isEnabled = sender.isOn
        default: break
        }
    }
}

extension FilterViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameFilterCell: filter.name.compareTo = textField.text ?? ""
        case dataFilterCell: filter.data.compareTo = textField.text ?? ""
        default: break
        }
        textField.resignFirstResponder()
        return false
    }
}
