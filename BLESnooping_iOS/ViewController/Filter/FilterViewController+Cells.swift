//
//  FilterViewController+Cells.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/14/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

extension FilterViewController {
    class TextFieldCell: UIView {
        private let stackView = UIStackView()
        let label = UILabel()
        // swiftlint:disable implicitly_unwrapped_optional
        var textField: UITextField!
        // swiftlint:enable implicitly_unwrapped_optional
        
        let toggleSwitch = SegmentSwitch()
        var toggleSwitchToggled: (() -> Void)?
        
        private var isHex: Bool = false
        
        init(isHex: Bool) {
            self.isHex = isHex
            super.init(frame: .zero)
            commonInit()
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            commonInit()
        }
        
        private func commonInit() {
            backgroundColor = .clear
            
            stackView.axis = .horizontal
            stackView.spacing = 8
            stackView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(stackView)
            stackView.pinEdgesToParent()
            stackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            label.text = "Label"
            label.font = UIFont.preferredFont(forTextStyle: .body)
            label.translatesAutoresizingMaskIntoConstraints = false
            let labelContainer = UIView()
            labelContainer.backgroundColor = .clear
            labelContainer.addSubview(label)
            label.leadingAnchor.constraint(equalTo: labelContainer.leadingAnchor, constant: 8).isActive = true
            label.topAnchor.constraint(equalTo: labelContainer.topAnchor, constant: 8).isActive = true
            label.trailingAnchor.constraint(equalTo: labelContainer.trailingAnchor, constant: -8).isActive = true
            label.bottomAnchor.constraint(equalTo: labelContainer.bottomAnchor, constant: -8).isActive = true
            stackView.addArrangedSubview(labelContainer)
            labelContainer.widthAnchor.constraint(equalToConstant: 76).isActive = true

            if isHex {
                textField = HexField()
            } else {
                textField = UITextField()
            }
            
            textField.borderStyle = .roundedRect
            textField.font = UIFont.preferredFont(forTextStyle: .body)
            textField.translatesAutoresizingMaskIntoConstraints = false
            stackView.addArrangedSubview(textField)

            let toggleSwitchContainer = UIView()
            toggleSwitchContainer.backgroundColor = .clear
            toggleSwitchContainer.addSubview(toggleSwitch)
            toggleSwitch.translatesAutoresizingMaskIntoConstraints = false
            toggleSwitch.centerYAnchor.constraint(equalTo: toggleSwitchContainer.centerYAnchor).isActive = true
            toggleSwitch.leadingAnchor.constraint(equalTo: toggleSwitchContainer.leadingAnchor, constant: 8).isActive = true
            toggleSwitch.trailingAnchor.constraint(equalTo: toggleSwitchContainer.trailingAnchor, constant: -8).isActive = true
            stackView.addArrangedSubview(toggleSwitchContainer)
        }
    }
    
    class StepperCell: UIView {
        private let stackView = UIStackView()
        let label = UILabel()
        let stepperValueLabel = UILabel()
        let stepper = UIStepper()
        let toggleSwitch = SegmentSwitch()
        var toggleSwitchToggled: (() -> Void)?
        
        init() {
            super.init(frame: .zero)
            commonInit()
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
            commonInit()
        }
        
        private func commonInit() {
            backgroundColor = .clear
            
            stackView.axis = .horizontal
            stackView.spacing = 8
            stackView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(stackView)
            stackView.pinEdgesToParent()
            stackView.heightAnchor.constraint(equalToConstant: 44).isActive = true
            
            label.text = "Label"
            label.font = UIFont.preferredFont(forTextStyle: .body)
            label.translatesAutoresizingMaskIntoConstraints = false
            let labelContainer = UIView()
            labelContainer.backgroundColor = .clear
            labelContainer.addSubview(label)
            label.leadingAnchor.constraint(equalTo: labelContainer.leadingAnchor, constant: 8).isActive = true
            label.topAnchor.constraint(equalTo: labelContainer.topAnchor, constant: 8).isActive = true
            label.trailingAnchor.constraint(equalTo: labelContainer.trailingAnchor, constant: -8).isActive = true
            label.bottomAnchor.constraint(equalTo: labelContainer.bottomAnchor, constant: -8).isActive = true
            stackView.addArrangedSubview(labelContainer)
            labelContainer.widthAnchor.constraint(equalToConstant: 76).isActive = true

            stepperValueLabel.textAlignment = .right
            stackView.addArrangedSubview(stepperValueLabel)
            
            stepper.maximumValue = 0
            stepper.minimumValue = -100
            
            let stepperContainer = UIView()
            stepperContainer.backgroundColor = .clear
            stepperContainer.addSubview(stepper)
            stepper.translatesAutoresizingMaskIntoConstraints = false
            stepper.centerYAnchor.constraint(equalTo: stepperContainer.centerYAnchor).isActive = true
            stepper.leadingAnchor.constraint(equalTo: stepperContainer.leadingAnchor, constant: 8).isActive = true
            stepper.trailingAnchor.constraint(equalTo: stepperContainer.trailingAnchor, constant: -8).isActive = true
            stackView.addArrangedSubview(stepperContainer)

            let toggleSwitchContainer = UIView()
            toggleSwitchContainer.backgroundColor = .clear
            toggleSwitchContainer.addSubview(toggleSwitch)
            toggleSwitch.translatesAutoresizingMaskIntoConstraints = false
            toggleSwitch.centerYAnchor.constraint(equalTo: toggleSwitchContainer.centerYAnchor).isActive = true
            toggleSwitch.leadingAnchor.constraint(equalTo: toggleSwitchContainer.leadingAnchor, constant: 8).isActive = true
            toggleSwitch.trailingAnchor.constraint(equalTo: toggleSwitchContainer.trailingAnchor, constant: -8).isActive = true
            stackView.addArrangedSubview(toggleSwitchContainer)
        }
    }
}
