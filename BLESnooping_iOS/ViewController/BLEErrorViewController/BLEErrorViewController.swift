//
//  BLEErrorViewController.swift
//  BLESnooping_iOS
//
//  Created by Zakk Hoyt on 4/9/20.
//  Copyright © 2020 Hatch Baby Inc. All rights reserved.
//

import UIKit
import BLESnoopingKit

class BLEProblemViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    
    struct Problem {
        let title: String
        let message: String
        let buttonTitle: String?
    }
    
    private let problem: Problem
    
    // MARK: Init
    
    required init?(coder: NSCoder, problem: Problem) {
        self.problem = problem
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = problem.title
        messageLabel.text = problem.message
        guard let buttonTitle = problem.buttonTitle else {
            button.isHidden = true
            return
        }
        button.isHidden = false
        button.setTitle(buttonTitle, for: .normal)
    }

    @IBAction
    func buttonTouchUpInside(_ sender: Any) {
        guard let url = URL(string: UIApplication.openSettingsURLString) else { return }
        UIApplication.shared.open(url)
    }
}

extension BLEProblemViewController: StoryboardInitializable {
    static var storyboardName: String = "BLEErrorViewController"
    static var storyboardIdentifier: String? = nil
}
