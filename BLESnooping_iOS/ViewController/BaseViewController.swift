//
//  BaseViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

extension CGFloat {
    static let alphaOff: CGFloat = 0.3
    static let alphaOn: CGFloat = 1.0
}

extension UILabel {
    func setTextOrUnavailable(_ text: String?) {
        guard let text = text else {
            self.text = kUnavailable
            alpha = CGFloat.alphaOff
            return
        }
        self.text = text
        alpha = CGFloat.alphaOn
    }
}

class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNotifiationObservers()
    }
    
    private func setupNotifiationObservers() {
        // Listen for this notifiction and present an alert box.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(bleScannerUnexpectedDisconnect),
                                               name: .BLEScannerUnexpectedDisconnect,
                                               object: nil)
    }
    
    @objc
    private func bleScannerUnexpectedDisconnect(note: Notification) {
        DispatchQueue.main.async {
            guard let device = note.userInfo?["device"] as? BLEDevice else {
                assertionFailure("device is nil")
                return
            }
            let seconds: TimeInterval? = {
                guard let initializedDate = device.initializedDate else { return nil }
                return Date().timeIntervalSince(initializedDate)
            }()
            let secondsString: String = {
                guard let seconds = seconds else { return kUnavailable }
                return String(format: "0.2f seconds", seconds)
            }()
            self.alert(title: "Error",
                       message: "Unexpectely disconnected from \(device.description) after \(secondsString)")
        }
    }
}

extension UIViewController {
    func alert(title: String?,
               message: String?,
               buttonTitles: [String],
               showCancelAction: Bool = false,
               completion: ((_ buttonIndex: Int?) -> Void)? = nil) {
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        
        for (index, buttonTitle) in buttonTitles.enumerated() {
            ac.addAction(UIAlertAction(title: buttonTitle, style: .default) { _ in
                completion?(index)
            })
        }
        
        if showCancelAction {
            ac.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
                completion?(nil)
            })
        }
        
        (navigationController ?? self).present(ac, animated: true, completion: nil)
    }
    
    func alert(title: String?,
               message: String?,
               buttonTitle: String = "OK",
               showCancelAction: Bool = false,
               completion: ((_ buttonIndex: Int?) -> Void)? = nil) {
        alert(title: title,
              message: message,
              buttonTitles: [buttonTitle],
              showCancelAction: showCancelAction,
              completion: completion)
    }
    
    func alertCollectPayload(title: String?,
                             message: String?,
                             completion: @escaping (String?) -> Void) {
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        
        ac.addTextField {
            $0.placeholder = "SCnnnnnnnn"
        }
        
        ac.addAction(UIAlertAction(title: "Write", style: .default) { _ in
            guard let text = ac.textFields?.first?.text else {
                completion(nil)
                return
            }
            completion(text)
        })
        
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel) { _ in
            completion(nil)
        })
        
        (navigationController ?? self).present(ac, animated: true, completion: nil)
    }
}
