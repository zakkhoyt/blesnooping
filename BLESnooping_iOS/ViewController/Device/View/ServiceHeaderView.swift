//
//  ServiceHeaderView.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class ServiceHeaderView: UITableViewHeaderFooterView {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var uuidLabel: UILabel!
    //@IBOutlet weak var manufacturerDataLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
}

extension ServiceHeaderView: TableViewReusable {
    static var reuseIdentifier: String {
        return "ServiceHeaderView"
    }
}
