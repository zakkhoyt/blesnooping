//
//  DeviceViewController.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
import CoreBluetooth

#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class DeviceViewController: BaseViewController {
    // MARK: Internal vars
    
    var device: BLEDevice
    
    private var reloadThrottle = ThrottleLatest<Float>()
    private lazy var connectBarb: UIBarButtonItem = {
        let barb = UIBarButtonItem(title: "Connect", style: .plain, target: self, action: #selector(connectBarbAction))
        return barb
    }()
    
    // MARK: IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: Init
    
    required init?(coder: NSCoder, device: BLEDevice) {
        self.device = device
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        device.initialize(delegate: self)
        let closeBarb = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeBarbAction))
        navigationItem.leftBarButtonItem = closeBarb
        navigationItem.rightBarButtonItem = connectBarb
        navigationItem.title = device.name
        updateConnectBarb()
        
        ServiceHeaderView.register(to: tableView)
        CharacteristicTableViewCell.register(to: tableView)
        
        reloadThrottle.start(duration: 1.0) { _ in
            self.tableView.reloadData()
        }
        
        setupNotificationObservers()
    }
    
    // MARK: Private funcs
    
    private func write(to characteristic: CBCharacteristic,
                       writeType: CBCharacteristicWriteType) {
        let vc = DataInputViewController.loadFromStoryboard()
        //let nc = UINavigationController(rootViewController: vc)
        present(vc, animated: true, completion: nil)
        
        vc.completion = { [weak self] data in
            guard let safeSelf = self else { return }
            vc.dismiss(animated: true, completion: nil)
            guard let data = data else { return }
            Logger.debug("Characteristic", "Writing 0x\(data.hexString) to characteristic \(characteristic.uuid.uuidString)")
            safeSelf.device.peripheral.writeValue(data, for: characteristic, type: writeType)
        }
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(scannerDidConnectToDevice),
                                               name: .BLEScannerDidConnectToDevice,
                                               object: device)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(scannerDidDisconnectFromDevice),
                                               name: .BLEScannerDidDisconnectFromDevice,
                                               object: device)
    }
    
    private func updateConnectBarb() {
        switch device.peripheral.state {
        case .connected: connectBarb.title = "Disconnect"
        default: connectBarb.title = "Connect"
        }
    }
    
    private func navigateToDataInputViewController() {
        let vc = DataInputViewController.loadFromStoryboard()
        let nc = UINavigationController(rootViewController: vc)
        present(nc, animated: true, completion: nil)
    }
    
    // MARK: IBActions
    @objc
    private func scannerDidConnectToDevice(note: Notification) {
        DispatchQueue.main.async {
            self.updateConnectBarb()
        }
    }

    @objc
    private func scannerDidDisconnectFromDevice(note: Notification) {
        DispatchQueue.main.async {
           self.updateConnectBarb()
        }
    }

    @objc
    private func closeBarbAction(sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @objc
    private func connectBarbAction(sender: UIBarButtonItem) {
        switch device.peripheral.state {
        case .disconnected:
            // Reconnect
            device.scanner.connect(to: device)
        default:
            // Disconnect and close the VC
            device.scanner.disconnect(from: device)
            navigationController?.dismiss(animated: true, completion: nil)
        }
    }
}

extension DeviceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        device.peripheral.services?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let service = device.peripheral.services?[section] else {
            preconditionFailure()
        }
        return service.characteristics?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CharacteristicTableViewCell.reuseIdentifier, for: indexPath) as! CharacteristicTableViewCell
        
        guard let service = device.peripheral.services?[indexPath.section] else {
            preconditionFailure()
        }
        guard let characteristics = service.characteristics else {
            preconditionFailure()
        }
        guard characteristics.count > indexPath.row else {
            preconditionFailure()
        }
        
        cell.indentationLevel = (indexPath.row + 1)
        
        let characteristic = characteristics[indexPath.row]
        let identifier = characteristic.uuid.uuidString
        cell.identifierLabel.text = "UUID: \(identifier)"
        
        if let name = BLEDevice.BLECharacteristic.nameDictionary[characteristic.uuid.uuidString] {
            cell.nameLabel.text = name
        } else {
            cell.nameLabel.text = "Unknown Characteristic"
        }
        
        let propertiesString: String = {
            //var output = ""
            var propertyStrings: [String] = []
            if characteristic.properties.contains(.read) {
                propertyStrings.append("Read")
            }
            
            if characteristic.properties.contains(.write) {
                propertyStrings.append("Write")
            }

            if characteristic.properties.contains(.writeWithoutResponse) {
                propertyStrings.append("Write (without response)")
            }
            
            if characteristic.properties.contains(.notify) {
                propertyStrings.append("Notify")
            }
            
            switch propertyStrings.count {
            case 0: return kUnavailable
            case 1: return propertyStrings.first ?? kUnavailable
            case 2..<100:
                var output = ""
                for (index, propertyString) in propertyStrings.enumerated() {
                    if index < propertyStrings.count - 2 {
                        output += "\(propertyString), "
                    } else if index < propertyStrings.count - 1 {
                        output += "\(propertyString) and "
                    } else {
                        output += propertyString
                    }
                }
                return output
            default: return kUnavailable
            }
        }()
        
        cell.propertiesLabel.text = "Properties: \(propertiesString)"
        
        cell.valueLabel.text = "Value: \(characteristic.valueString)"
        
        cell.valueSentLabel.text = "Value Sent: N/A"
        
        let descriptorsString: String? = {
            (characteristic.descriptors ?? [])
                .compactMap { $0.summaryString }
                .joined(separator: "\n")
        }()
        
        if let descriptorsString = descriptorsString {
            cell.descriptorsLabel.text = "\(descriptorsString)"
        } else {
            cell.descriptorsLabel.text = nil
        }
        
        cell.readButton.isHidden = true
        cell.writeButton.isHidden = true
        cell.notifyButton.isHidden = true
        
        if characteristic.properties.contains(.read) {
            cell.readButton.isHidden = false
            cell.readButtonTapped = { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.device.read(characteristic: characteristic)
            }
        }
        
        if characteristic.properties.contains(.write) {
            cell.writeButton.isHidden = false
            cell.writeButtonTapped = { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.write(to: characteristic, writeType: .withResponse)
            }
        }
        
        if characteristic.properties.contains(.writeWithoutResponse) {
            cell.writeButton.isHidden = false
            cell.writeButtonTapped = { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.write(to: characteristic, writeType: .withoutResponse)
            }
        }

        if characteristic.properties.contains(.notify) {
            cell.notifyButton.isHidden = false
            
            if characteristic.isNotifying {
                cell.notifyButton.setTitle("Notifying", for: .normal)
            } else {
                cell.notifyButton.setTitle("Notify", for: .normal)
            }
            
            cell.notifyButtonTapped = { [weak self] in
                guard let safeSelf = self else { return }
                safeSelf.device.peripheral.setNotifyValue(!characteristic.isNotifying, for: characteristic)
            }
        }

        cell.indentationLevel = 2
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}

extension DeviceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        44
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let services = device.peripheral.services else { return nil }
        let service = services[section]
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: ServiceHeaderView.reuseIdentifier) as! ServiceHeaderView
        view.uuidLabel.text = "UUID: \(service.uuid.uuidString)"
        if let name = BLEDevice.BLEService.nameDictionary[service.uuid.uuidString] {
            view.nameLabel.text = name
        } else {
            view.nameLabel.text = "Unknown Service"
        }
        view.countLabel.text = "\(service.characteristics?.count ?? 0) characteristics"
        return view
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension DeviceViewController: BLEDeviceDelegate {
    private func reloadTableView() {
        reloadThrottle.throttle(value: 0)
    }
    
    func bleDeviceDidInitialize(_ device: BLEDevice) {
        PublicUserDefaults.writePreviousDevice(device: device)
        self.reloadTableView()
    }
    
    func bleDevicedidUpdatePeripheralName(_ device: BLEDevice) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didDiscoverServices services: [CBService]) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didDiscoverCharacteristics characteristics: [CBCharacteristic]) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didRead data: Data, fromCharacteristic characteristic: CBCharacteristic) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didWrite data: Data, toCharacteristic characteristic: CBCharacteristic) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didUpdateNotificationStateFor characteristic: CBCharacteristic) {
        self.reloadTableView()
    }
    
    func bleDevice(_ device: BLEDevice, didDiscoverDescriptorsFor characteristic: CBCharacteristic) {
        self.reloadTableView()
    }
}

extension DeviceViewController: StoryboardInitializable {
    static var storyboardName: String = "DeviceViewController"
    static var storyboardIdentifier: String? = nil
}
