//
//  CharacteristicTableViewCell.swift
//  BLESnooping
//
//  Created by Zakk Hoyt on 4/1/20.
//  Copyright © 2020 Zakk Hoyt Inc. All rights reserved.
//

import UIKit
#if os(tvOS)
import BLESnoopingKit_tvOS
#else
import BLESnoopingKit_iOS
#endif

class CharacteristicTableViewCell: UITableViewCell {
    var readButtonTapped: (() -> Void)?
    var writeButtonTapped: (() -> Void)?
    var notifyButtonTapped: (() -> Void)?
    
    @IBOutlet weak var indentConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var identifierLabel: UILabel!
    @IBOutlet weak var propertiesLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var valueSentLabel: UILabel!
    @IBOutlet weak var maximumWriteLengthLabel: UILabel!
    
    @IBOutlet weak var descriptorsLabel: UILabel!
    
    @IBOutlet weak var readButton: UIButton!
    @IBOutlet weak var writeButton: UIButton!
    @IBOutlet weak var notifyButton: UIButton!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        readButton.isHidden = true
        writeButton.isHidden = true
        notifyButton.isHidden = true
    }
    
    override var indentationLevel: Int {
        get {
            return super.indentationLevel
        }
        set {
            super.indentationLevel = newValue
            indentConstraint.constant = CGFloat(8 + newValue * 8)
        }
    }
    
    override var canBecomeFocused: Bool {
        false //!(readButton.isHidden && writeButton.isHidden && notifyButton.isHidden)
    }
    
    @IBAction
    func readButtonTouchUpInside(_ sender: UIButton) {
        readButtonTapped?()
    }
    
    @IBAction
    func writeButtonTouchUpInside(_ sender: Any) {
        writeButtonTapped?()
    }
    
    @IBAction
    func notifyButtonTouchUpInside(_ sender: Any) {
        notifyButtonTapped?()
    }
}

extension CharacteristicTableViewCell: TableViewReusable {
    static var reuseIdentifier: String {
        return "CharacteristicTableViewCell"
    }
}
