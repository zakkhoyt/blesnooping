//
//  PromptTransitionCoordinator.swift
//  PromptMenu
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

private let kDuration: TimeInterval = 0.3

class PromptTransitionCoordinator: NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        PromptPresenterController(presentedViewController: presented,
                                  presenting: presenting)
    }
    
    public func animationController(forPresented presented: UIViewController,
                                    presenting: UIViewController,
                                    source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        AlertPresentationAnimator()
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        AlertDismissAnimator()
    }
}

class PromptPresenterController: UIPresentationController {
    public override init(presentedViewController: UIViewController,
                         presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
    }
    
    override var adaptivePresentationStyle: UIModalPresentationStyle { .custom }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        let safeFrame = presentingViewController.view.frame.inset(by: presentingViewController.view.safeAreaInsets)
        let xInset: CGFloat =  presentingViewController.view.frame.width * 0.1 / -2
        let yInset: CGFloat = presentingViewController.view.safeAreaInsets.top
        return CGRect(x: safeFrame.origin.x + xInset,
                      y: safeFrame.origin.y + yInset,
                      width: (safeFrame.width - 2 * xInset),
                      height: (safeFrame.width - 2 * xInset))
    }
    
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
}

class AlertPresentationAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { kDuration }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toVC = transitionContext.viewController(forKey: .to)!
        let fromVC = transitionContext.viewController(forKey: .from)!
        
        let containerView = transitionContext.containerView
        let animationDuration = transitionDuration(using: transitionContext)
        
        toVC.view.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
        toVC.view.alpha = 0
        toVC.view.layer.masksToBounds = true
        toVC.view.layer.cornerRadius = 16
        
        containerView.addSubview(toVC.view)
        
        fromVC.view.layer.masksToBounds = true
        fromVC.view.layer.cornerRadius = 16
        
        UIView.animate(withDuration: animationDuration,
                       animations: { // [weak self] in
                        toVC.view.transform = CGAffineTransform.identity
                        toVC.view.alpha = 1.0
                        
                        fromVC.view.alpha = 0.25
                        fromVC.view.transform = CGAffineTransform.identity.scaledBy(x: 0.9, y: 0.9)
        }, completion: { finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}

class AlertDismissAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval { kDuration }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toVC = transitionContext.viewController(forKey: .to)!
        let fromVC = transitionContext.viewController(forKey: .from)!
        
        let containerView = transitionContext.containerView
        let animationDuration = transitionDuration(using: transitionContext)
        
        fromVC.view.transform = CGAffineTransform.identity
        
        containerView.addSubview(fromVC.view)
        
        UIView.animate(withDuration: animationDuration,
                       animations: { // [weak self] in
                        fromVC.view.transform = CGAffineTransform.identity.scaledBy(x: 0.5, y: 0.5)
                        fromVC.view.alpha = 0.0
                        
                        toVC.view.transform = CGAffineTransform.identity
                        toVC.view.alpha = 1
        }, completion: { finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            fromVC.view.layer.masksToBounds = false
            fromVC.view.layer.cornerRadius = 0
            toVC.view.layer.masksToBounds = false
            toVC.view.layer.cornerRadius = 0
        })
    }
}
