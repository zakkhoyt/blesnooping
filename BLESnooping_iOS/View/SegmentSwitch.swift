//
//  SegmentSwitch.swift
//  SegmentSwitch
//
//  Created by Zakk Hoyt on 4/10/20.
//  Copyright © 2020 Vaporwarewolf. All rights reserved.
//

import UIKit

class SegmentSwitch: UIControl {
    var isOn: Bool = true {
        didSet {
            switch isOn {
            case false:
                segment.selectedSegmentTintColor = nil
                segment.selectedSegmentIndex = 0
            case true:
                segment.selectedSegmentTintColor = tintColor
                segment.selectedSegmentIndex = 1
            }
        }
    }
    
    override var intrinsicContentSize: CGSize { CGSize(width: 72, height: 40) }

    private let segment = UISegmentedControl()
    
    init() {
        super.init(frame: .zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit() {
        tintColor = .systemGreen
        backgroundColor = .clear
        
        // Add subview
        segment.selectedSegmentTintColor = tintColor
        segment.backgroundColor = .darkGray
        segment.addTarget(self, action: #selector(segmentValueChanged), for: .valueChanged)
        segment.translatesAutoresizingMaskIntoConstraints = false
        addSubview(segment)
        segment.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        segment.topAnchor.constraint(equalTo: topAnchor).isActive = true
        segment.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        segment.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        
        // Set up states
        segment.removeAllSegments()
        segment.insertSegment(withTitle: "Off", at: 0, animated: false)
        segment.insertSegment(withTitle: "On", at: 1, animated: false)
        segment.selectedSegmentIndex = isOn ? 1 : 0
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        layoutSubviews()
    }
    
    @objc
    private func segmentValueChanged(sender: UISegmentedControl) {
        //guard let onState = State(rawValue: sender.selectedSegmentIndex) else { return }
        
        //self.onState = onState
        switch sender.selectedSegmentIndex {
        case 0: isOn = false
        default: isOn = true
        }
        sendActions(for: .valueChanged)
    }
}
