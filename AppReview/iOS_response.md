
I have published all associated web site URLs. (Main site, privacy, support). 

In regard to your questions:

- Is authorisation required from the devices the app connects to? 
    No, authorization is never required for BLE connections which lack pairing. 

- How is the authorization obtained?
    No authorization is obtained. If the target device does not allow BLE connections, then no connection will take place. 

- What modifications does the app perform on devices it connects to?
    This app will read/write to/from CBCharacteristics using information that the user inputs. No data is written otherwise. 

To be concise, this app is just a BLE device browser and is a show case app for me (an iOS developer with a hardware background). 

I don't feel that a demo video is necessary for this app. It is very simple to use. No account required. Just open and use. If you demand a demo video, then I will make one. Please consider. 
